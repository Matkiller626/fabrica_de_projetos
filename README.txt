Projeto - Planet Sports


Software de sistema de uma loja para artigos esportivos desenvolvido na disciplina de F�brica de Projetos II, na FACENS - Sorocaba, SP.

P.O. do projeto: Matheus Antunes Camargo; Team: Anna Jullia Barbosa, Leonardo Lerich, Maurilho Baldini de Moraes, Rafael Henrique Carvalho; Scrum Master: Hudson Machado.

Tecnologias a serem utilizadas no projeto:

O software foi desenvolvido e Java e o banco de dados utilizado foi do tipo SQL.

O projeto utilizar� a metodologia Scrum Agile, com o apoio da ferramenta Jira para gerenciamento de projeto e GitLab para versionamento do Projeto.

Features:

[X] - Cadastro de Usuario
[X] - Cadastro de Cliente
[X] - Cadastro de Produto
[ ] - Venda
[X] - Catalogo com Filtro
[X] - Alteracao de Cadastros



