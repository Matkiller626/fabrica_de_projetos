
package planet_sports;

import Conexao.MySQL;
import Objetos.Cliente;
import Objetos.Endereco;
import java.awt.Color;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;



public class TelaConsultaClientes extends javax.swing.JFrame {

    MySQL conectar = new MySQL();
    Cliente ClienteBuscado = new Cliente();
    Endereco EnderecoBuscado = new Endereco();
    boolean FormPreenchido = false;
    boolean TelaEditavel = false;
    JOptionPane PopUp = new JOptionPane();
    String User;
    String Permissao;
    
  public TelaConsultaClientes(String User, String Permissao) {
    initComponents();
    this.User = User;
    this.Permissao = Permissao;
    BtnDeletar.setVisible(false);
  }
    
    public TelaConsultaClientes() {
        initComponents();
    }


    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        Background1 = new javax.swing.JPanel();
        PnlTitulo = new javax.swing.JPanel();
        LblConsultaClinete = new javax.swing.JLabel();
        PnlPesquisa = new javax.swing.JPanel();
        TxtPesqDocumento = new javax.swing.JTextField();
        BtnConsultar = new javax.swing.JButton();
        BtnVoltar = new javax.swing.JButton();
        LblPesqDocumento = new javax.swing.JLabel();
        PnlInfoCliente = new javax.swing.JPanel();
        LblNome = new javax.swing.JLabel();
        LblDocumento = new javax.swing.JLabel();
        LblTelefone = new javax.swing.JLabel();
        TxtTelefone = new javax.swing.JTextField();
        TxtDocumento = new javax.swing.JTextField();
        TxtNome = new javax.swing.JTextField();
        LblDataNascimento = new javax.swing.JLabel();
        TxtDataNascimento = new javax.swing.JTextField();
        PnlEndereco = new javax.swing.JPanel();
        LblEndereco = new javax.swing.JLabel();
        TxtEndereco = new javax.swing.JTextField();
        TxtBairro = new javax.swing.JTextField();
        LblBairro = new javax.swing.JLabel();
        LblEstado = new javax.swing.JLabel();
        CBoxEstado = new javax.swing.JComboBox<>();
        LblCidade = new javax.swing.JLabel();
        TxtCidade = new javax.swing.JTextField();
        LblNumero = new javax.swing.JLabel();
        TxtNumero = new javax.swing.JTextField();
        BtnDeletar = new javax.swing.JButton();
        PnlBotoes = new javax.swing.JPanel();
        BtnEditar = new javax.swing.JButton();
        BtnLimpar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        Background1.setBackground(new java.awt.Color(255, 204, 102));
        Background1.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        PnlTitulo.setBackground(new java.awt.Color(255, 204, 102));

        LblConsultaClinete.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        LblConsultaClinete.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblConsultaClinete.setText("CONSULTA DE CLIENTE");
        LblConsultaClinete.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        javax.swing.GroupLayout PnlTituloLayout = new javax.swing.GroupLayout(PnlTitulo);
        PnlTitulo.setLayout(PnlTituloLayout);
        PnlTituloLayout.setHorizontalGroup(
            PnlTituloLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PnlTituloLayout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(LblConsultaClinete, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(0, 0, 0))
        );
        PnlTituloLayout.setVerticalGroup(
            PnlTituloLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PnlTituloLayout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(LblConsultaClinete)
                .addGap(0, 0, 0))
        );

        PnlPesquisa.setBackground(new java.awt.Color(255, 204, 102));

        TxtPesqDocumento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TxtPesqDocumentoActionPerformed(evt);
            }
        });

        BtnConsultar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/planet_sports/Icons/Icone.Lupa2.png"))); // NOI18N
        BtnConsultar.setText("Consultar");
        BtnConsultar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnConsultarActionPerformed(evt);
            }
        });

        BtnVoltar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/planet_sports/Icons/Icone.Voltar.png"))); // NOI18N
        BtnVoltar.setText("Voltar");
        BtnVoltar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnVoltarActionPerformed(evt);
            }
        });

        LblPesqDocumento.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        LblPesqDocumento.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        LblPesqDocumento.setText("CPF");
        LblPesqDocumento.setPreferredSize(new java.awt.Dimension(65, 17));

        javax.swing.GroupLayout PnlPesquisaLayout = new javax.swing.GroupLayout(PnlPesquisa);
        PnlPesquisa.setLayout(PnlPesquisaLayout);
        PnlPesquisaLayout.setHorizontalGroup(
            PnlPesquisaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PnlPesquisaLayout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(LblPesqDocumento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(TxtPesqDocumento, javax.swing.GroupLayout.PREFERRED_SIZE, 184, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(BtnConsultar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(BtnVoltar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        PnlPesquisaLayout.setVerticalGroup(
            PnlPesquisaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PnlPesquisaLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(PnlPesquisaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(PnlPesquisaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(LblPesqDocumento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(TxtPesqDocumento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(PnlPesquisaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(BtnConsultar)
                        .addComponent(BtnVoltar)))
                .addContainerGap())
        );

        PnlInfoCliente.setBackground(new java.awt.Color(255, 204, 102));

        LblNome.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        LblNome.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        LblNome.setText("Nome");
        LblNome.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        LblNome.setPreferredSize(new java.awt.Dimension(65, 17));

        LblDocumento.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        LblDocumento.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        LblDocumento.setText("RG/CPF");
        LblDocumento.setPreferredSize(new java.awt.Dimension(65, 17));

        LblTelefone.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        LblTelefone.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        LblTelefone.setText("Telefone");
        LblTelefone.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        LblTelefone.setPreferredSize(new java.awt.Dimension(65, 17));

        TxtTelefone.setBackground(new java.awt.Color(204, 204, 204));
        TxtTelefone.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TxtTelefoneActionPerformed(evt);
            }
        });

        TxtDocumento.setBackground(new java.awt.Color(204, 204, 204));
        TxtDocumento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TxtDocumentoActionPerformed(evt);
            }
        });

        TxtNome.setBackground(new java.awt.Color(204, 204, 204));
        TxtNome.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TxtNomeActionPerformed(evt);
            }
        });

        LblDataNascimento.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        LblDataNascimento.setText("Data de Nascimento");
        LblDataNascimento.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);

        TxtDataNascimento.setBackground(new java.awt.Color(204, 204, 204));
        TxtDataNascimento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TxtDataNascimentoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout PnlInfoClienteLayout = new javax.swing.GroupLayout(PnlInfoCliente);
        PnlInfoCliente.setLayout(PnlInfoClienteLayout);
        PnlInfoClienteLayout.setHorizontalGroup(
            PnlInfoClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, PnlInfoClienteLayout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addGroup(PnlInfoClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(LblTelefone, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(PnlInfoClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(LblDocumento, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(LblNome, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(PnlInfoClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(PnlInfoClienteLayout.createSequentialGroup()
                        .addGroup(PnlInfoClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(TxtDocumento, javax.swing.GroupLayout.DEFAULT_SIZE, 106, Short.MAX_VALUE)
                            .addComponent(TxtTelefone))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(LblDataNascimento)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(TxtDataNascimento, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(TxtNome))
                .addContainerGap())
        );
        PnlInfoClienteLayout.setVerticalGroup(
            PnlInfoClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, PnlInfoClienteLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(PnlInfoClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(LblNome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(TxtNome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(PnlInfoClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(TxtDocumento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(LblDataNascimento)
                    .addComponent(LblDocumento, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(TxtDataNascimento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(PnlInfoClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(LblTelefone, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(TxtTelefone, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        PnlEndereco.setBackground(new java.awt.Color(255, 204, 102));

        LblEndereco.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        LblEndereco.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        LblEndereco.setText("Endereço");
        LblEndereco.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);

        TxtEndereco.setBackground(new java.awt.Color(204, 204, 204));
        TxtEndereco.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TxtEnderecoActionPerformed(evt);
            }
        });

        TxtBairro.setBackground(new java.awt.Color(204, 204, 204));
        TxtBairro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TxtBairroActionPerformed(evt);
            }
        });

        LblBairro.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        LblBairro.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        LblBairro.setText("Bairro");
        LblBairro.setPreferredSize(new java.awt.Dimension(65, 17));

        LblEstado.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        LblEstado.setText("UF");

        CBoxEstado.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "", "AC", "AL", "AP", "AM", "BA", "CE", "DF", "ES", "GO", "MA", "MT", "MS", "MG", "PA", "PB", "PR", "PE", "PI", "RR", "RO", "RJ", "RN", "RS", "SC", "SP", "SE", "TO" }));

        LblCidade.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        LblCidade.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        LblCidade.setText("Cidade");
        LblCidade.setPreferredSize(new java.awt.Dimension(65, 17));

        TxtCidade.setBackground(new java.awt.Color(204, 204, 204));
        TxtCidade.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TxtCidadeActionPerformed(evt);
            }
        });

        LblNumero.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        LblNumero.setText("n°");

        TxtNumero.setBackground(new java.awt.Color(204, 204, 204));
        TxtNumero.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TxtNumero.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TxtNumeroActionPerformed(evt);
            }
        });

        BtnDeletar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/planet_sports/Icons/Icone.Deletar.png"))); // NOI18N
        BtnDeletar.setText("Deletar");
        BtnDeletar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnDeletarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout PnlEnderecoLayout = new javax.swing.GroupLayout(PnlEndereco);
        PnlEndereco.setLayout(PnlEnderecoLayout);
        PnlEnderecoLayout.setHorizontalGroup(
            PnlEnderecoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PnlEnderecoLayout.createSequentialGroup()
                .addGroup(PnlEnderecoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(LblCidade, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(LblEndereco, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(LblBairro, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(PnlEnderecoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(PnlEnderecoLayout.createSequentialGroup()
                        .addComponent(TxtEndereco)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(LblNumero)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(TxtNumero, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, PnlEnderecoLayout.createSequentialGroup()
                        .addGroup(PnlEnderecoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(TxtBairro)
                            .addComponent(TxtCidade))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(LblEstado)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(CBoxEstado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(32, 32, 32)
                        .addComponent(BtnDeletar, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(27, 27, 27))))
        );
        PnlEnderecoLayout.setVerticalGroup(
            PnlEnderecoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PnlEnderecoLayout.createSequentialGroup()
                .addGroup(PnlEnderecoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(PnlEnderecoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(LblNumero)
                        .addComponent(TxtNumero, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(PnlEnderecoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(LblEndereco)
                        .addComponent(TxtEndereco, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(PnlEnderecoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(PnlEnderecoLayout.createSequentialGroup()
                        .addGroup(PnlEnderecoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(TxtBairro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(LblBairro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(PnlEnderecoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(PnlEnderecoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(LblEstado)
                                .addComponent(CBoxEstado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(PnlEnderecoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(LblCidade, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(TxtCidade, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addComponent(BtnDeletar, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        PnlBotoes.setBackground(new java.awt.Color(255, 204, 102));

        BtnEditar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/planet_sports/Icons/Icone.Editar.png"))); // NOI18N
        BtnEditar.setText("Editar");
        BtnEditar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnEditarActionPerformed(evt);
            }
        });

        BtnLimpar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/planet_sports/Icons/Icone.Limpar.png"))); // NOI18N
        BtnLimpar.setText("Limpar");
        BtnLimpar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnLimparActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout PnlBotoesLayout = new javax.swing.GroupLayout(PnlBotoes);
        PnlBotoes.setLayout(PnlBotoesLayout);
        PnlBotoesLayout.setHorizontalGroup(
            PnlBotoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PnlBotoesLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(BtnEditar, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, Short.MAX_VALUE)
                .addComponent(BtnLimpar, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        PnlBotoesLayout.setVerticalGroup(
            PnlBotoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PnlBotoesLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(PnlBotoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(BtnEditar)
                    .addComponent(BtnLimpar))
                .addContainerGap())
        );

        javax.swing.GroupLayout Background1Layout = new javax.swing.GroupLayout(Background1);
        Background1.setLayout(Background1Layout);
        Background1Layout.setHorizontalGroup(
            Background1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(Background1Layout.createSequentialGroup()
                .addGroup(Background1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(PnlBotoes, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, Background1Layout.createSequentialGroup()
                        .addContainerGap(19, Short.MAX_VALUE)
                        .addGroup(Background1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(PnlTitulo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(PnlPesquisa, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(PnlEndereco, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(PnlInfoCliente, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap())
        );
        Background1Layout.setVerticalGroup(
            Background1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, Background1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(PnlTitulo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(PnlPesquisa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(PnlInfoCliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(PnlEndereco, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(PnlBotoes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(Background1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(Background1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void TxtPesqDocumentoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TxtPesqDocumentoActionPerformed
        if(TelaEditavel){
            setTelaConsulta();
            LimparFormulario();
        }
        
        BuscarCliente(ClienteBuscado);
    }//GEN-LAST:event_TxtPesqDocumentoActionPerformed

    private void TxtNomeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TxtNomeActionPerformed
        
    }//GEN-LAST:event_TxtNomeActionPerformed

    private void TxtDocumentoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TxtDocumentoActionPerformed
        
    }//GEN-LAST:event_TxtDocumentoActionPerformed

    private void BtnConsultarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnConsultarActionPerformed
        
        if(TelaEditavel){
            setTelaConsulta();
            LimparFormulario();
        }
        
        BuscarCliente(ClienteBuscado);
        
    }//GEN-LAST:event_BtnConsultarActionPerformed

    private void BtnEditarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnEditarActionPerformed
        
        if(TelaEditavel){
            if(isInfoOK()){
               AtualizaObjetoEndereco(EnderecoBuscado);
               EditarCliente(ClienteBuscado, EnderecoBuscado);
            }
        }else{
            if(FormPreenchido){
                setTelaEditavel();
            }else{
               JOptionPane.showMessageDialog(null, "Nenhum cliente selecionado."); 
            }
        }
        
        
        
    }//GEN-LAST:event_BtnEditarActionPerformed

    private void TxtTelefoneActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TxtTelefoneActionPerformed
        
    }//GEN-LAST:event_TxtTelefoneActionPerformed

    private void TxtNumeroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TxtNumeroActionPerformed
        
    }//GEN-LAST:event_TxtNumeroActionPerformed

    private void TxtCidadeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TxtCidadeActionPerformed
        
    }//GEN-LAST:event_TxtCidadeActionPerformed

    private void TxtBairroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TxtBairroActionPerformed
        
    }//GEN-LAST:event_TxtBairroActionPerformed

    private void TxtEnderecoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TxtEnderecoActionPerformed
        
    }//GEN-LAST:event_TxtEnderecoActionPerformed

    private void BtnVoltarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnVoltarActionPerformed
        MainScreen Menu = new MainScreen(User, Permissao);
        Menu.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_BtnVoltarActionPerformed

    private void BtnLimparActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnLimparActionPerformed
        
        if(TelaEditavel){
            setTelaConsulta();
            LimparFormulario();
        }else{
            LimparFormulario();
        }
        
    }//GEN-LAST:event_BtnLimparActionPerformed

    private void BtnDeletarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnDeletarActionPerformed
        
        Object Opcoes[] = {"Sim", "Cancelar"};
                
        if(PopupOpcao(Opcoes)){
            DeletarCliente(ClienteBuscado);
        }
        
    }//GEN-LAST:event_BtnDeletarActionPerformed

    private void TxtDataNascimentoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TxtDataNascimentoActionPerformed
       
    }//GEN-LAST:event_TxtDataNascimentoActionPerformed
    
    private void BuscarCliente(Cliente ClienteBuscado){
        this.conectar.conectaBanco();
        
        String DocDigitado = TxtPesqDocumento.getText();
                
        try {
            this.conectar.executarSQL(
                   "SELECT "
                    + "NomeCompleto,"                    
                    + "Documento,"
                    + "Nascimento,"
                    + "Telefone,"
                    + "Rua,"
                    + "Numero,"                    
                    + "Bairro,"
                    + "Cidade,"
                    + "UF"
                 + " FROM"
                     + " Cliente"
                 + " JOIN Endereco on IdEndereco = Endereco_id"
                 + " WHERE Documento = '" + DocDigitado + "';"
            );
            
            while(this.conectar.getResultSet().next()){
                ClienteBuscado.setNomeCompleto(this.conectar.getResultSet().getString(1));
                ClienteBuscado.setDocumento(this.conectar.getResultSet().getString(2));
                ClienteBuscado.setDataNascimento(this.conectar.getResultSet().getString(3));
                ClienteBuscado.setTelefone(this.conectar.getResultSet().getString(4));
                EnderecoBuscado.setRua(this.conectar.getResultSet().getString(5));
                EnderecoBuscado.setNumero(this.conectar.getResultSet().getInt(6));
                EnderecoBuscado.setBairro(this.conectar.getResultSet().getString(7));
                EnderecoBuscado.setCidade(this.conectar.getResultSet().getString(8));
                EnderecoBuscado.setUF(this.conectar.getResultSet().getString(9));
           }
                
           if(ClienteBuscado.getNomeCompleto() == null){
                LimparFormulario();
                JOptionPane.showMessageDialog(null, "Cliente não encontrado!");
           }else{
               FormPreenchido = true;
               BtnDeletar.setVisible(true);
           }
           
        } catch (Exception e) {            
            System.out.println("Erro ao consultar cliente " +  e.getMessage());
            JOptionPane.showMessageDialog(null, "Erro ao buscar cliente");
            
        }finally{
            TxtPesqDocumento.setText("");
            TxtNome.setText(ClienteBuscado.getNomeCompleto());
            TxtDocumento.setText(ClienteBuscado.getDocumento());
            TxtDataNascimento.setText(ClienteBuscado.getDataNascimento());
            TxtTelefone.setText(ClienteBuscado.getTelefone());
            TxtEndereco.setText(EnderecoBuscado.getRua());
            TxtNumero.setText(String.valueOf(EnderecoBuscado.getNumero()));
            TxtBairro.setText(EnderecoBuscado.getBairro());
            TxtCidade.setText(EnderecoBuscado.getCidade());
            CBoxEstado.setSelectedItem(EnderecoBuscado.getUF());
            this.conectar.fechaBanco();   
        }               
    }
    
    private void LimparFormulario(){
        TxtBairro.setText("");
        TxtCidade.setText("");
        TxtDataNascimento.setText("");
        TxtDocumento.setText("");
        TxtEndereco.setText("");
        TxtNome.setText("");
        TxtPesqDocumento.setText("");
        TxtTelefone.setText("");
        TxtNumero.setText("");
        CBoxEstado.setSelectedIndex(0);
        FormPreenchido = false;
        setTelaConsulta();
        LimparCliente();
    }
    
    private void LimparCliente(){
        
        ClienteBuscado.setNomeCompleto(null);
        ClienteBuscado.setDocumento(null);
        ClienteBuscado.setDataNascimento(null);
        ClienteBuscado.setTelefone(null);
        EnderecoBuscado.setRua(null);
        EnderecoBuscado.setNumero(0);
        EnderecoBuscado.setBairro(null);
        EnderecoBuscado.setCidade(null);
        EnderecoBuscado.setUF(null);
        
    }
    
    private void setTelaEditavel(){
        TelaEditavel = true;
        TxtNome.setBackground(new Color(255,255,255));
        TxtBairro.setBackground(new Color(255,255,255));
        TxtCidade.setBackground(new Color(255,255,255));
        TxtDataNascimento.setBackground(new Color(255,255,255));
        TxtDocumento.setBackground(new Color(255,255,255));
        TxtEndereco.setBackground(new Color(255,255,255));
        TxtNome.setBackground(new Color(255,255,255));
        TxtTelefone.setBackground(new Color(255,255,255));
        TxtNumero.setBackground(new Color(255,255,255));
        TxtPesqDocumento.setBackground(new Color(204,204,204));

        BtnEditar.setText("Atualizar");
        BtnEditar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/planet_sports/Icons/Icone.Atualizar.png")));
        BtnLimpar.setText("Cancelar");
        BtnDeletar.setVisible(false);
    }
    
    private void setTelaConsulta(){
        TelaEditavel = false;
        TxtNome.setBackground(new Color(204,204,204));
        TxtBairro.setBackground(new Color(204,204,204));
        TxtCidade.setBackground(new Color(204,204,204));
        TxtDataNascimento.setBackground(new Color(204,204,204));
        TxtDocumento.setBackground(new Color(204,204,204));
        TxtEndereco.setBackground(new Color(204,204,204));
        TxtNome.setBackground(new Color(204,204,204));
        TxtTelefone.setBackground(new Color(204,204,204));
        TxtNumero.setBackground(new Color(204,204,204));
        TxtPesqDocumento.setBackground(new Color(255,255,255));

        BtnEditar.setText("Editar");
        BtnEditar.setIcon(new ImageIcon("/planet_sports/Icons/Icone.Editar.png"));
        BtnLimpar.setText("Limpar");
        BtnDeletar.setVisible(false);
    }
    
    private void AtualizaObjetoEndereco(Endereco EnderecoBuscado){
        
        EnderecoBuscado.setRua(TxtEndereco.getText());
        EnderecoBuscado.setNumero(Integer.valueOf(TxtNumero.getText()));
        EnderecoBuscado.setBairro(TxtBairro.getText());
        EnderecoBuscado.setCidade(TxtCidade.getText());
        EnderecoBuscado.setUF(String.valueOf(CBoxEstado.getSelectedItem()));
        
    }
    
    public void EditarCliente(Cliente ClienteBuscado, Endereco EnderecoBuscado){
        this.conectar.conectaBanco();
        
        int idEndereco = EnderecoExiste(EnderecoBuscado);
        
        
        if(idEndereco >= 0){
            while (idEndereco == 0){
                CadastraEndereco(EnderecoBuscado);
                idEndereco = EnderecoExiste(EnderecoBuscado);
            }
        
            this.conectar.conectaBanco();
            
            try {
                this.conectar.updateSQL(
                    "UPDATE Cliente SET "                    
                        + "NomeCompleto = '" + TxtNome.getText() + "',"
                        + "Documento = '" + TxtDocumento.getText() + "',"
                        + "Nascimento = '" + TxtDataNascimento.getText() + "',"
                        + "Telefone = '" + TxtTelefone.getText() + "',"                   
                        + "Endereco_id = '" + idEndereco + "'"
                    + " WHERE "
                        + "Documento = '" + ClienteBuscado.getDocumento() + "'"
                    + ";"
                );
            }catch(Exception e){
                System.out.println("Erro ao atualizar cliente " +  e.getMessage());
                JOptionPane.showMessageDialog(null, "Erro ao atualizar cliente");
            }finally{
                this.conectar.fechaBanco();
                LimparFormulario();
                setTelaConsulta();
                JOptionPane.showMessageDialog(null, "Cliente atualizado com sucesso");
            }
        }
    }
    
    private void DeletarCliente(Cliente ClienteBuscado){
        this.conectar.conectaBanco();
        
        
        try {            
            this.conectar.updateSQL(
                "DELETE FROM Cliente "
                + " WHERE "
                    + "Documento = '" + ClienteBuscado.getDocumento() + "'"
                + ";"            
            );
            
        } catch (Exception e) {
            System.out.println("Erro ao deletar cliente " +  e.getMessage());
            JOptionPane.showMessageDialog(null, "Erro ao deletar cliente");
        }finally{
            this.conectar.fechaBanco();
            LimparFormulario();
            setTelaConsulta();
            JOptionPane.showMessageDialog(null, "Cliente deletado com sucesso");            
        }     
        
    }
    
    private void CadastraEndereco(Endereco NovoEndereco){
    this.conectar.conectaBanco();
    
    try{
        this.conectar.insertSQL("insert into Endereco values (" +
                                "null"                        + "," +
                                "'" + NovoEndereco.getRua()    + "'," +
                                "'" + NovoEndereco.getNumero() + "'," +
                                "'" + NovoEndereco.getBairro() + "'," +
                                "'" + NovoEndereco.getCidade() + "'," +
                                "'" + NovoEndereco.getUF()     + "');");
    }catch(Exception e){
        System.out.println("Erro ao cadastrar Endereco " +  e.getMessage());
        JOptionPane.showMessageDialog(null, "Erro ao cadastrar Endereco");
    }
  }
    
    private int EnderecoExiste(Endereco EnderecoBuscado){
      this.conectar.conectaBanco();
      String RuaEncontrada  = "";
      int idEncontrado = 0;
      int NumeroEncontrado = -1;
      
        
        try {
           this.conectar.executarSQL("select * from Endereco where Rua = " + "'" + EnderecoBuscado.getRua() + "'" 
                  + "and Numero = " + "'" + EnderecoBuscado.getNumero() + "'" + ";"); //SQL para buscar no Banco
                      
           
           while(this.conectar.getResultSet().next()){
               idEncontrado  =  this.conectar.getResultSet().getInt(1);
               RuaEncontrada =  this.conectar.getResultSet().getString(2);
               NumeroEncontrado  =  this.conectar.getResultSet().getInt(3);
           }
           
           if(RuaEncontrada.equals(EnderecoBuscado.getRua())){
               if (EnderecoBuscado.getNumero() == NumeroEncontrado){
                   this.conectar.fechaBanco();
                   return idEncontrado;
               } else{
                    this.conectar.fechaBanco();
                    return 0;
               }
           } else{
               this.conectar.fechaBanco();
               return 0;
           }
           
        }catch(Exception e){
            System.out.println("Erro ao verificar Endereco " +  e.getMessage());
            JOptionPane.showMessageDialog(null, "Erro ao verificar Endereco");
            this.conectar.fechaBanco();
            return -1;
        }
   }
    
    private boolean isInfoOK(){
        
        String Mensagem = "";
        String NomeCompleto = TxtNome.getText();
        String DataNascimento = TxtDataNascimento.getText();
        String Documento = TxtDocumento.getText();
        String Endereco = TxtEndereco.getText();
        String Bairro = TxtBairro.getText();
        String Cidade = TxtCidade.getText();
        String Telefone = TxtTelefone.getText();
        
        int TamanhoDocumento  = Documento.length();
        int TamanhoTelefone   = Telefone.length();
        
        if( NomeCompleto.equals("")     ||
            Documento.equals("")        ||
            DataNascimento.equals("")   ||
            Endereco.equals("")         ||
            Bairro.equals("")           ||
            Cidade.equals("")){
            
            Mensagem = Mensagem + "Preencha todos os campos.\n";            
        }
        
        if (TamanhoDocumento < 8 || TamanhoDocumento > 11) {
            Mensagem = Mensagem + "Documento inválido.\n";
            TxtDocumento.setText("");
        }
        
        if (!TxtNumero.getText().equals("")){
            if(!isStringInt(TxtNumero.getText())){
                Mensagem = Mensagem + "Numero de endereco invalido.\n";
                TxtNumero.setText("");
            }else{
                if(Integer.valueOf(TxtNumero.getText()) < 0){
                    Mensagem = Mensagem + "Numero de endereco invalido.\n";
                    TxtNumero.setText(""); 
                }                
            }
        }
        
        if(!Telefone.equals("")){
            if (TamanhoTelefone < 8 || TamanhoTelefone > 9){
                Mensagem = Mensagem + "Numero de telefone invalido.\n";
                TxtTelefone.setText("");
            }
        }
        
        if (!Mensagem.equals("")){
            JOptionPane.showMessageDialog(this, Mensagem);
            return false;
        }else {
            return true;
        }
    }
    
    public boolean isStringInt(String s){

          try {
              Integer.parseInt(s);
              return true;
          } catch (NumberFormatException ex) {
              return false;
          }
      } 
    
    private boolean PopupOpcao(Object Opcoes[]){
        int Escolha;
        
        Escolha = PopUp.showOptionDialog(this, "Deseja deletar o cliente?", "Prestes a deletar um cliente!",
        PopUp.YES_NO_OPTION, PopUp.QUESTION_MESSAGE,null,Opcoes,
        Opcoes[0]);
        
        return Escolha == 0;
    }
    
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(TelaConsultaClientes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(TelaConsultaClientes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(TelaConsultaClientes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(TelaConsultaClientes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new TelaConsultaClientes().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel Background1;
    private javax.swing.JButton BtnConsultar;
    private javax.swing.JButton BtnDeletar;
    private javax.swing.JButton BtnEditar;
    private javax.swing.JButton BtnLimpar;
    private javax.swing.JButton BtnVoltar;
    private javax.swing.JComboBox<String> CBoxEstado;
    private javax.swing.JLabel LblBairro;
    private javax.swing.JLabel LblCidade;
    private javax.swing.JLabel LblConsultaClinete;
    private javax.swing.JLabel LblDataNascimento;
    private javax.swing.JLabel LblDocumento;
    private javax.swing.JLabel LblEndereco;
    private javax.swing.JLabel LblEstado;
    private javax.swing.JLabel LblNome;
    private javax.swing.JLabel LblNumero;
    private javax.swing.JLabel LblPesqDocumento;
    private javax.swing.JLabel LblTelefone;
    private javax.swing.JPanel PnlBotoes;
    private javax.swing.JPanel PnlEndereco;
    private javax.swing.JPanel PnlInfoCliente;
    private javax.swing.JPanel PnlPesquisa;
    private javax.swing.JPanel PnlTitulo;
    private javax.swing.JTextField TxtBairro;
    private javax.swing.JTextField TxtCidade;
    private javax.swing.JTextField TxtDataNascimento;
    private javax.swing.JTextField TxtDocumento;
    private javax.swing.JTextField TxtEndereco;
    private javax.swing.JTextField TxtNome;
    private javax.swing.JTextField TxtNumero;
    private javax.swing.JTextField TxtPesqDocumento;
    private javax.swing.JTextField TxtTelefone;
    // End of variables declaration//GEN-END:variables
}
