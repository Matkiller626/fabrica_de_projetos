
package planet_sports;

import Conexao.MySQL;
import Objetos.Usuario;
import javax.swing.JOptionPane;

public class LoginScreen extends javax.swing.JFrame {
     
    Usuario Usuario = new Usuario();
    MySQL conectar = new MySQL();
    
    public LoginScreen() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        LbltBemVindo = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        TxtFaleConosco = new javax.swing.JTextArea();
        LblUser = new javax.swing.JLabel();
        TxtUser = new javax.swing.JTextField();
        LblSenha = new javax.swing.JLabel();
        TxtPassword = new javax.swing.JPasswordField();
        LblLoginIcon = new javax.swing.JLabel();
        LblSenhaIcon = new javax.swing.JLabel();
        BtnEntrar = new javax.swing.JButton();
        BtnCadastro = new javax.swing.JButton();
        LblLogo = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMaximumSize(new java.awt.Dimension(850, 450));
        setMinimumSize(new java.awt.Dimension(850, 450));
        setResizable(false);
        setSize(new java.awt.Dimension(850, 450));

        jPanel1.setBackground(new java.awt.Color(255, 204, 102));
        jPanel1.setMaximumSize(new java.awt.Dimension(850, 450));
        jPanel1.setMinimumSize(new java.awt.Dimension(850, 450));
        jPanel1.setPreferredSize(new java.awt.Dimension(850, 450));

        LbltBemVindo.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        LbltBemVindo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LbltBemVindo.setText("Seja bem-vindo a plataforma Planet Sports!");

        TxtFaleConosco.setColumns(20);
        TxtFaleConosco.setFont(new java.awt.Font("Monospaced", 0, 12)); // NOI18N
        TxtFaleConosco.setRows(5);
        TxtFaleConosco.setText("Problemas com o login?\nFavor comunicar o setor de TI para\nsolucionar problema.\nTelefone: (15) 3238-1188\nWhatsApp: (15) 98154-7777");
        jScrollPane1.setViewportView(TxtFaleConosco);

        LblUser.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        LblUser.setText("Login Usuário");

        TxtUser.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        TxtUser.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TxtUserActionPerformed(evt);
            }
        });

        LblSenha.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        LblSenha.setText("Senha");

        TxtPassword.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        TxtPassword.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TxtPasswordActionPerformed(evt);
            }
        });

        LblLoginIcon.setBackground(new java.awt.Color(255, 204, 102));
        LblLoginIcon.setIcon(new javax.swing.ImageIcon(getClass().getResource("/planet_sports/Icons/Icone.Login.png"))); // NOI18N

        LblSenhaIcon.setIcon(new javax.swing.ImageIcon(getClass().getResource("/planet_sports/Icons/Icone.Senha.png"))); // NOI18N

        BtnEntrar.setText("Entrar");
        BtnEntrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnEntrarActionPerformed(evt);
            }
        });

        BtnCadastro.setText("Cadastrar");
        BtnCadastro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnCadastroActionPerformed(evt);
            }
        });

        LblLogo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/planet_sports/Icons/Icone.LogoGrande.png"))); // NOI18N

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(LbltBemVindo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(LblLogo)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(LblSenhaIcon)
                            .addComponent(LblLoginIcon))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(BtnCadastro, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(TxtPassword)
                            .addComponent(BtnEntrar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(TxtUser)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 391, Short.MAX_VALUE)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(LblUser)
                                    .addComponent(LblSenha))
                                .addGap(0, 0, Short.MAX_VALUE)))))
                .addGap(18, 18, 18))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addComponent(LbltBemVindo, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(LblUser)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(TxtUser, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(LblLoginIcon, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(LblSenha)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(TxtPassword, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(LblSenhaIcon))
                        .addGap(18, 18, 18)
                        .addComponent(BtnEntrar)
                        .addGap(18, 18, 18)
                        .addComponent(BtnCadastro)
                        .addGap(13, 13, 13)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(LblLogo))
                .addContainerGap(49, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        jPanel1.getAccessibleContext().setAccessibleName("");

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void TxtPasswordActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TxtPasswordActionPerformed
        RealizarLogin();
    }//GEN-LAST:event_TxtPasswordActionPerformed

  private void TxtUserActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TxtUserActionPerformed
        RealizarLogin();
  }//GEN-LAST:event_TxtUserActionPerformed

  private void BtnEntrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnEntrarActionPerformed
        RealizarLogin();
  }//GEN-LAST:event_BtnEntrarActionPerformed

    private void BtnCadastroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnCadastroActionPerformed
        TelaCadastroUsuario CadastroUsuario = new TelaCadastroUsuario();
        CadastroUsuario.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_BtnCadastroActionPerformed
    
    private void RealizarLogin(){
        Usuario.setUsername(TxtUser.getText());
        Usuario.setSenha(String.valueOf(TxtPassword.getPassword()));      
    
        if (VerificaCredencial(Usuario)){
            JOptionPane.showMessageDialog(this, "Bem vindo "+Usuario.getUsername()+"!" );
            MainScreen MenuPrincipal = new MainScreen(Usuario);
            MenuPrincipal.setVisible(true);
            this.dispose();
        }else{
          JOptionPane.showMessageDialog(this, "Usuario ou senha incorreto!");
        }
    }
    
    private boolean VerificaCredencial(Usuario Usuario){
        this.conectar.conectaBanco();
        String SenhaEncontrada = null;
        
        try {
           this.conectar.executarSQL("select * from Usuario where Username = " + "'" + Usuario.getUsername() + "'" + ";"); //SQL para buscar UserDigitado no banco
           
           while(this.conectar.getResultSet().next()){
                SenhaEncontrada   = this.conectar.getResultSet().getString(3);// 1 é o ID, 2 o username, 3 senha, 4 permissao
                Usuario.setPermissao(this.conectar.getResultSet().getInt(1));
           }
           
           if (SenhaEncontrada.equals(Usuario.getSenha())){
               this.conectar.fechaBanco();
               return true;
           } else {
               this.conectar.fechaBanco();
               return false;
           }
           
        }catch(Exception e){
            System.out.println("Erro ao verificar usuario e senha " +  e.getMessage());
            this.conectar.fechaBanco();
            return false;
        }
    }
    
    public static void main(String args[]) {
        
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(LoginScreen.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(LoginScreen.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(LoginScreen.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(LoginScreen.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new LoginScreen().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton BtnCadastro;
    private javax.swing.JButton BtnEntrar;
    private javax.swing.JLabel LblLoginIcon;
    private javax.swing.JLabel LblLogo;
    private javax.swing.JLabel LblSenha;
    private javax.swing.JLabel LblSenhaIcon;
    private javax.swing.JLabel LblUser;
    private javax.swing.JLabel LbltBemVindo;
    private javax.swing.JTextArea TxtFaleConosco;
    private javax.swing.JPasswordField TxtPassword;
    private javax.swing.JTextField TxtUser;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables
}
