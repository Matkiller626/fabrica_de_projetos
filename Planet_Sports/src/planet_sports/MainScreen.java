
package planet_sports;

import Objetos.Usuario;
import javax.swing.JOptionPane;

public class MainScreen extends javax.swing.JFrame {
    
    JOptionPane PopUp = new JOptionPane();
    String User;
    String Permissao;
    
    public MainScreen(String User, String Permissao) {
        initComponents();
        this.User = User;
        this.Permissao = Permissao;
        LblUsuarioLogado.setText(User);
        LblTipoPerfil.setText(Permissao);
    }
    
    public MainScreen() {
        initComponents();
    }
    
    public MainScreen(Usuario UserLogado) {
        initComponents();
        User = UserLogado.getUsername();
        if(UserLogado.getPermissao() == 1){
            Permissao = "Perfil Administrador";
        }else{
            Permissao = "Perfil Utilizador";
        }
        LblUsuarioLogado.setText(User);
        LblTipoPerfil.setText(Permissao);
        
    }


    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        BtnCadastroCliente = new javax.swing.JButton();
        LblTipoPerfil = new javax.swing.JLabel();
        LblUsuarioLogado = new javax.swing.JLabel();
        BtnCadastroProduto = new javax.swing.JButton();
        BtnVendaProduto = new javax.swing.JButton();
        BtnCatalogo = new javax.swing.JButton();
        BtnLogout = new javax.swing.JButton();
        LblPlanetSportIcon = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        jPanel1.setBackground(new java.awt.Color(255, 204, 102));
        jPanel1.setLayout(null);

        BtnCadastroCliente.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        BtnCadastroCliente.setIcon(new javax.swing.ImageIcon(getClass().getResource("/planet_sports/Icons/Icone.Clientes.png"))); // NOI18N
        BtnCadastroCliente.setText("Clientes");
        BtnCadastroCliente.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        BtnCadastroCliente.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        BtnCadastroCliente.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        BtnCadastroCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnCadastroClienteActionPerformed(evt);
            }
        });
        jPanel1.add(BtnCadastroCliente);
        BtnCadastroCliente.setBounds(580, 60, 170, 120);

        LblTipoPerfil.setText("Perfil");
        jPanel1.add(LblTipoPerfil);
        LblTipoPerfil.setBounds(20, 30, 160, 14);

        LblUsuarioLogado.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        LblUsuarioLogado.setText("User");
        jPanel1.add(LblUsuarioLogado);
        LblUsuarioLogado.setBounds(20, 10, 180, 17);

        BtnCadastroProduto.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        BtnCadastroProduto.setIcon(new javax.swing.ImageIcon(getClass().getResource("/planet_sports/Icons/Icone.Cadastro.png"))); // NOI18N
        BtnCadastroProduto.setText("Produtos");
        BtnCadastroProduto.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        BtnCadastroProduto.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        BtnCadastroProduto.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        BtnCadastroProduto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnCadastroProdutoActionPerformed(evt);
            }
        });
        jPanel1.add(BtnCadastroProduto);
        BtnCadastroProduto.setBounds(580, 210, 170, 120);

        BtnVendaProduto.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        BtnVendaProduto.setIcon(new javax.swing.ImageIcon(getClass().getResource("/planet_sports/Icons/Icone.Vendas.png"))); // NOI18N
        BtnVendaProduto.setText("Caixa");
        BtnVendaProduto.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        BtnVendaProduto.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        BtnVendaProduto.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        BtnVendaProduto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnVendaProdutoActionPerformed(evt);
            }
        });
        jPanel1.add(BtnVendaProduto);
        BtnVendaProduto.setBounds(20, 60, 170, 120);

        BtnCatalogo.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        BtnCatalogo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/planet_sports/Icons/Icone.Catalogo.png"))); // NOI18N
        BtnCatalogo.setText("Catálogo");
        BtnCatalogo.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        BtnCatalogo.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        BtnCatalogo.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        BtnCatalogo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnCatalogoActionPerformed(evt);
            }
        });
        jPanel1.add(BtnCatalogo);
        BtnCatalogo.setBounds(20, 210, 170, 120);

        BtnLogout.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        BtnLogout.setIcon(new javax.swing.ImageIcon(getClass().getResource("/planet_sports/Icons/Icone.Logout.png"))); // NOI18N
        BtnLogout.setText("Logout");
        BtnLogout.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnLogoutActionPerformed(evt);
            }
        });
        jPanel1.add(BtnLogout);
        BtnLogout.setBounds(300, 320, 170, 50);

        LblPlanetSportIcon.setIcon(new javax.swing.ImageIcon(getClass().getResource("/planet_sports/Icons/Icone.LogoGrande.png"))); // NOI18N
        jPanel1.add(LblPlanetSportIcon);
        LblPlanetSportIcon.setBounds(200, 7, 362, 310);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 770, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 387, Short.MAX_VALUE)
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void BtnCadastroClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnCadastroClienteActionPerformed
      
        Object Opcoes[] = {"Cadastrar Cliente", "Consultar Cliente"};
                
        if(PopupOpcao(Opcoes)){
            TelaCadastroCliente CadastroCliente = new TelaCadastroCliente(User,Permissao);
            CadastroCliente.setVisible(true);
            this.dispose();
        }else {
            TelaConsultaClientes ConsultaCliente = new TelaConsultaClientes(User,Permissao);
            ConsultaCliente.setVisible(true);
            this.dispose();
        }
        
    }//GEN-LAST:event_BtnCadastroClienteActionPerformed

    private void BtnCadastroProdutoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnCadastroProdutoActionPerformed
        
        Object Opcoes[] = {"Cadastrar Produto", "Consultar Produto"};
                
        if(PopupOpcao(Opcoes)){
            TelaCadastroProduto CadastroProduto = new TelaCadastroProduto(User,Permissao);
            CadastroProduto.setVisible(true);
            this.dispose();
        }else {
            TelaConsultaProduto ConsultaProduto = new TelaConsultaProduto(User,Permissao);
            ConsultaProduto.setVisible(true);
            this.dispose();
        }
        
    }//GEN-LAST:event_BtnCadastroProdutoActionPerformed

    private void BtnVendaProdutoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnVendaProdutoActionPerformed
        TelaVenda Caixa = new TelaVenda(User,Permissao);
        Caixa.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_BtnVendaProdutoActionPerformed

    private void BtnCatalogoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnCatalogoActionPerformed
        TelaCatalogo Catalogo = new TelaCatalogo(User,Permissao);
        Catalogo.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_BtnCatalogoActionPerformed

    private void BtnLogoutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnLogoutActionPerformed
        LoginScreen Login = new LoginScreen();
        Login.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_BtnLogoutActionPerformed

    private boolean PopupOpcao(Object Opcoes[]){
        int Escolha;
        
        Escolha = PopUp.showOptionDialog(this, "Escolha uma opcao", "Escolha uma opcao",
        PopUp.YES_NO_OPTION, PopUp.QUESTION_MESSAGE,null,Opcoes,
        Opcoes[0]);
        
        return Escolha == 0;
    }
    
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainScreen.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainScreen.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainScreen.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainScreen.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MainScreen().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton BtnCadastroCliente;
    private javax.swing.JButton BtnCadastroProduto;
    private javax.swing.JButton BtnCatalogo;
    private javax.swing.JButton BtnLogout;
    private javax.swing.JButton BtnVendaProduto;
    private javax.swing.JLabel LblPlanetSportIcon;
    private javax.swing.JLabel LblTipoPerfil;
    private javax.swing.JLabel LblUsuarioLogado;
    private javax.swing.JPanel jPanel1;
    // End of variables declaration//GEN-END:variables
}
