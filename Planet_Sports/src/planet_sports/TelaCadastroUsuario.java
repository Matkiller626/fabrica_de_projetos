
package planet_sports;

import Objetos.Usuario;
import Conexao.MySQL;
import javax.swing.JOptionPane;

public class TelaCadastroUsuario extends javax.swing.JFrame {

    MySQL conectar = new MySQL();
    Usuario NovoUsuario = new Usuario();
    
  public TelaCadastroUsuario() {
    initComponents();
  }


  @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        BtnGroup = new javax.swing.ButtonGroup();
        PnlBackground = new javax.swing.JPanel();
        LblTituloCadastro = new javax.swing.JLabel();
        LblUsuario = new javax.swing.JLabel();
        TxtUsuario = new javax.swing.JTextField();
        LblSenha = new javax.swing.JLabel();
        PassField = new javax.swing.JPasswordField();
        LblConfirmaSenha = new javax.swing.JLabel();
        PassFieldConfirm = new javax.swing.JPasswordField();
        BtnCadastrar = new javax.swing.JButton();
        BtnVoltar = new javax.swing.JButton();
        Administrador = new javax.swing.JRadioButton();
        Funcionario = new javax.swing.JRadioButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        PnlBackground.setBackground(new java.awt.Color(255, 204, 102));

        LblTituloCadastro.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        LblTituloCadastro.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblTituloCadastro.setText("Cadastro de Usuario");
        LblTituloCadastro.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        LblUsuario.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        LblUsuario.setText("Usuario");

        LblSenha.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        LblSenha.setText("Senha");

        LblConfirmaSenha.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        LblConfirmaSenha.setText("Confirme sua senha");

        BtnCadastrar.setText("Cadastrar");
        BtnCadastrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnCadastrarActionPerformed(evt);
            }
        });

        BtnVoltar.setText("Voltar");
        BtnVoltar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnVoltarActionPerformed(evt);
            }
        });

        Administrador.setBackground(new java.awt.Color(255, 204, 102));
        BtnGroup.add(Administrador);
        Administrador.setText("Administrador");

        Funcionario.setBackground(new java.awt.Color(255, 204, 102));
        BtnGroup.add(Funcionario);
        Funcionario.setText("Funcionario");

        javax.swing.GroupLayout PnlBackgroundLayout = new javax.swing.GroupLayout(PnlBackground);
        PnlBackground.setLayout(PnlBackgroundLayout);
        PnlBackgroundLayout.setHorizontalGroup(
            PnlBackgroundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PnlBackgroundLayout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addGroup(PnlBackgroundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(PnlBackgroundLayout.createSequentialGroup()
                        .addComponent(LblUsuario)
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, PnlBackgroundLayout.createSequentialGroup()
                        .addGroup(PnlBackgroundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(PassFieldConfirm, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(PassField, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(TxtUsuario)
                            .addComponent(LblTituloCadastro, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(BtnCadastrar, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(BtnVoltar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, PnlBackgroundLayout.createSequentialGroup()
                                .addGroup(PnlBackgroundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(LblConfirmaSenha, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(LblSenha, javax.swing.GroupLayout.Alignment.LEADING))
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, PnlBackgroundLayout.createSequentialGroup()
                                .addComponent(Administrador)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(Funcionario)))
                        .addGap(18, 18, 18))))
        );
        PnlBackgroundLayout.setVerticalGroup(
            PnlBackgroundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PnlBackgroundLayout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addComponent(LblTituloCadastro)
                .addGap(24, 24, 24)
                .addComponent(LblUsuario)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(TxtUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(LblSenha)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(PassField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(LblConfirmaSenha)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(PassFieldConfirm, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(PnlBackgroundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Administrador)
                    .addComponent(Funcionario))
                .addGap(18, 18, 18)
                .addComponent(BtnCadastrar)
                .addGap(18, 18, 18)
                .addComponent(BtnVoltar)
                .addGap(18, 18, 18))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(PnlBackground, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(PnlBackground, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void BtnCadastrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnCadastrarActionPerformed
        
        if(isInfoOK()){//Se as informações estiverem ok, cadastra o usuario
            CadastrarUsuario(NovoUsuario);
        }
        
    }//GEN-LAST:event_BtnCadastrarActionPerformed

    private void BtnVoltarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnVoltarActionPerformed
        LoginScreen Login = new LoginScreen();
        Login.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_BtnVoltarActionPerformed
    
    private void CadastrarUsuario(Usuario NovoUsuario){
        
        this.conectar.conectaBanco();
        
        NovoUsuario.setUsername(TxtUsuario.getText());
        NovoUsuario.setSenha(String.valueOf(PassField.getPassword()));
        
        if (Administrador.isSelected()){
            NovoUsuario.setPermissao(1);            
        }else if (Funcionario.isSelected()){
            NovoUsuario.setPermissao(0);
        }
        
        try{
            
         this.conectar.insertSQL("insert into Usuario values ("
                    +       "null,"
                    + "'" + NovoUsuario.getUsername() + "',"
                    + "'" + NovoUsuario.getSenha() + "',"
                    +       NovoUsuario.getPermissao()
                + ");");  
            
        }catch(Exception e){
            System.out.println("Erro ao cadastrar usuario " +  e.getMessage());
            JOptionPane.showMessageDialog(null, "Erro ao cadastrar usuario");
        }finally{            
            this.conectar.fechaBanco();
            JOptionPane.showMessageDialog(null, "Usuario cadastrado com sucesso");
            LimparFormulario();
        }
    }
    
    private boolean isInfoOK(){
        
        String Mensagem = "";
        String UserDigitado = TxtUsuario.getText();
        int TamanhoUser = UserDigitado.length();
        String SenhaDigitada = String.valueOf(PassField.getPassword());
        int TamanhoSenha = SenhaDigitada.length();
        String ConfirmaSenha = String.valueOf(PassFieldConfirm.getPassword());
        
        if (UsuarioExiste(UserDigitado)) {
            Mensagem = Mensagem + "Nome de usuario nao disponivel\n";
            TxtUsuario.setText("");
        }
        
        if (TamanhoUser < 4 || TamanhoUser > 15) {//Pre requisito para usuario
            Mensagem = Mensagem + "Nome de usuario deve ser ter entre 4 e 15 caracteres\n";
            TxtUsuario.setText("");
        }
        
        if (TamanhoSenha < 4 || TamanhoSenha > 15) {
            Mensagem = Mensagem + "Senha deve conter entre 4 e 15 caracteres\n";
            PassField.setText("");
            PassFieldConfirm.setText("");
        }          
        
        if (!ConfirmaSenha.equals(SenhaDigitada)){
            Mensagem = Mensagem + "Campos Senha e Confirme sua senha devem ser iguais\n";
            PassField.setText("");
            PassFieldConfirm.setText("");
        }
        
        if(!Administrador.isSelected() && !Funcionario.isSelected()){
            Mensagem = Mensagem + "Selecione o tipo de usuario\n";
            BtnGroup.clearSelection();
        }        
        
        if (!Mensagem.equals("")){
            JOptionPane.showMessageDialog(this, Mensagem);
            return false;
        }else {
            return true;
        }
    }
    
    private boolean UsuarioExiste(String UserDigitado){
        this.conectar.conectaBanco();
        String UserEncontrado = "";
        
        try {
           this.conectar.executarSQL("select * from Usuario where Username = " + "'" + UserDigitado + "'" + ";"); //SQL para buscar UserDigitado no banco
           
           while(this.conectar.getResultSet().next()){
               UserEncontrado = this.conectar.getResultSet().getString(2);// 1 é o ID, 2 o username.
           }
           
           if(UserEncontrado.equals(UserDigitado)){ // Se o user for encontrado, retorna verdadeiro
               this.conectar.fechaBanco();
               return true;
           } else{ //Se nao, retorna falso
               this.conectar.fechaBanco();
               return false;
           }
           
        }catch(Exception e){
            System.out.println("Erro ao verificar usuario " +  e.getMessage());
            JOptionPane.showMessageDialog(null, "Erro ao verificar usuário");
            this.conectar.fechaBanco();
            return true;
        }
    }
    
    private void LimparFormulario(){
        TxtUsuario.setText("");
        PassField.setText("");
        PassFieldConfirm.setText("");
        BtnGroup.clearSelection();
    }
    
  public static void main(String args[]) {
    /* Set the Nimbus look and feel */
    //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
    /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
     */
    try {
      for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
        if ("Nimbus".equals(info.getName())) {
          javax.swing.UIManager.setLookAndFeel(info.getClassName());
          break;
        }
      }
    } catch (ClassNotFoundException ex) {
      java.util.logging.Logger.getLogger(TelaCadastroUsuario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
    } catch (InstantiationException ex) {
      java.util.logging.Logger.getLogger(TelaCadastroUsuario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
    } catch (IllegalAccessException ex) {
      java.util.logging.Logger.getLogger(TelaCadastroUsuario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
    } catch (javax.swing.UnsupportedLookAndFeelException ex) {
      java.util.logging.Logger.getLogger(TelaCadastroUsuario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
    }
    //</editor-fold>


    java.awt.EventQueue.invokeLater(new Runnable() {
      public void run() {
        new TelaCadastroUsuario().setVisible(true);
      }
    });
  }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JRadioButton Administrador;
    private javax.swing.JButton BtnCadastrar;
    private javax.swing.ButtonGroup BtnGroup;
    private javax.swing.JButton BtnVoltar;
    private javax.swing.JRadioButton Funcionario;
    private javax.swing.JLabel LblConfirmaSenha;
    private javax.swing.JLabel LblSenha;
    private javax.swing.JLabel LblTituloCadastro;
    private javax.swing.JLabel LblUsuario;
    private javax.swing.JPasswordField PassField;
    private javax.swing.JPasswordField PassFieldConfirm;
    private javax.swing.JPanel PnlBackground;
    private javax.swing.JTextField TxtUsuario;
    // End of variables declaration//GEN-END:variables
}
