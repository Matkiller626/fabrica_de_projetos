
package planet_sports;

import Conexao.MySQL;
import Objetos.Produto;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;


public class TelaVenda extends javax.swing.JFrame {
    
    MySQL conectar = new MySQL();
    Produto Produto = new Produto();
    float TotalCompra = 0;
    JOptionPane PopUp = new JOptionPane();
    String User;
    String Permissao;
    
  public TelaVenda(String User, String Permissao) {
    initComponents();
    this.User = User;
    this.Permissao = Permissao;
    CarrinhoCompra.getColumnModel().getColumn(1).setMinWidth(0);
    CarrinhoCompra.setAutoResizeMode(CarrinhoCompra.AUTO_RESIZE_LAST_COLUMN);
    CarrinhoCompra.getColumnModel().getColumn(0).setPreferredWidth(140);
    CarrinhoCompra.getColumnModel().getColumn(1).setPreferredWidth(10);
    CarrinhoCompra.getColumnModel().getColumn(2).setPreferredWidth(45);
    CarrinhoCompra.getColumnModel().getColumn(3).setMaxWidth(Integer.MAX_VALUE);
  }
    
  public TelaVenda() {
    initComponents();
  }

  @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        VendaBackground = new javax.swing.JPanel();
        PnlNota = new javax.swing.JPanel();
        ScrollVenda = new javax.swing.JScrollPane();
        CarrinhoCompra = new javax.swing.JTable();
        PnlCodPesquisa = new javax.swing.JPanel();
        LblCodigo = new javax.swing.JLabel();
        TxtCodigo = new javax.swing.JTextField();
        LblTotalVenda = new javax.swing.JLabel();
        LblTotalVendaN = new javax.swing.JLabel();
        IconPlanetSports = new javax.swing.JLabel();
        BtnPesquisar = new javax.swing.JButton();
        BtnVoltar = new javax.swing.JButton();
        BtnCancelarVenda = new javax.swing.JButton();
        PnlInfoProduto = new javax.swing.JPanel();
        LblNomeProduto = new javax.swing.JLabel();
        LblQnt = new javax.swing.JLabel();
        LblPrecoUnitario = new javax.swing.JLabel();
        LblTotalItem = new javax.swing.JLabel();
        LblTotalItemN = new javax.swing.JLabel();
        LblPrecoUnitarioN = new javax.swing.JLabel();
        TxtQuantidade = new javax.swing.JTextField();
        BtnConfirmaProduto = new javax.swing.JButton();
        BtnCancelarItem = new javax.swing.JButton();
        BtnExcluirItem = new javax.swing.JButton();
        BtnFecharVenda = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(255, 204, 102));
        setResizable(false);

        VendaBackground.setBackground(new java.awt.Color(255, 204, 102));

        PnlNota.setBackground(new java.awt.Color(255, 255, 255));

        CarrinhoCompra.setBackground(new java.awt.Color(255, 204, 102));
        CarrinhoCompra.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Produto", "Qnt", "Preço", "Total"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        CarrinhoCompra.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        CarrinhoCompra.getTableHeader().setReorderingAllowed(false);
        ScrollVenda.setViewportView(CarrinhoCompra);

        javax.swing.GroupLayout PnlNotaLayout = new javax.swing.GroupLayout(PnlNota);
        PnlNota.setLayout(PnlNotaLayout);
        PnlNotaLayout.setHorizontalGroup(
            PnlNotaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(ScrollVenda, javax.swing.GroupLayout.DEFAULT_SIZE, 352, Short.MAX_VALUE)
        );
        PnlNotaLayout.setVerticalGroup(
            PnlNotaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(ScrollVenda, javax.swing.GroupLayout.DEFAULT_SIZE, 345, Short.MAX_VALUE)
        );

        PnlCodPesquisa.setBackground(new java.awt.Color(255, 204, 102));

        LblCodigo.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        LblCodigo.setText("Código");

        TxtCodigo.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        TxtCodigo.setAutoscrolls(false);
        TxtCodigo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TxtCodigoActionPerformed(evt);
            }
        });

        LblTotalVenda.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        LblTotalVenda.setText("Total Venda");

        LblTotalVendaN.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        LblTotalVendaN.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblTotalVendaN.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 1, true));
        LblTotalVendaN.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        IconPlanetSports.setIcon(new javax.swing.ImageIcon(getClass().getResource("/planet_sports/Icons/Icone.LogoPequeno.png"))); // NOI18N

        BtnPesquisar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/planet_sports/Icons/Icone.Lupa2.png"))); // NOI18N
        BtnPesquisar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnPesquisarActionPerformed(evt);
            }
        });

        BtnVoltar.setText("Voltar");
        BtnVoltar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnVoltarActionPerformed(evt);
            }
        });

        BtnCancelarVenda.setText("Cancelar Venda");
        BtnCancelarVenda.setPreferredSize(new java.awt.Dimension(107, 30));
        BtnCancelarVenda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnCancelarVendaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout PnlCodPesquisaLayout = new javax.swing.GroupLayout(PnlCodPesquisa);
        PnlCodPesquisa.setLayout(PnlCodPesquisaLayout);
        PnlCodPesquisaLayout.setHorizontalGroup(
            PnlCodPesquisaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PnlCodPesquisaLayout.createSequentialGroup()
                .addGroup(PnlCodPesquisaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(PnlCodPesquisaLayout.createSequentialGroup()
                        .addComponent(TxtCodigo)
                        .addGap(2, 2, 2)
                        .addComponent(BtnPesquisar, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(LblTotalVendaN, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(IconPlanetSports, javax.swing.GroupLayout.DEFAULT_SIZE, 242, Short.MAX_VALUE)
                    .addGroup(PnlCodPesquisaLayout.createSequentialGroup()
                        .addGroup(PnlCodPesquisaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(LblTotalVenda)
                            .addComponent(LblCodigo))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(PnlCodPesquisaLayout.createSequentialGroup()
                        .addComponent(BtnVoltar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(18, 18, 18)
                        .addComponent(BtnCancelarVenda, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        PnlCodPesquisaLayout.setVerticalGroup(
            PnlCodPesquisaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PnlCodPesquisaLayout.createSequentialGroup()
                .addGroup(PnlCodPesquisaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(BtnVoltar, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(BtnCancelarVenda, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(IconPlanetSports, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(LblCodigo)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(PnlCodPesquisaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(BtnPesquisar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(TxtCodigo))
                .addGap(18, 18, 18)
                .addComponent(LblTotalVenda)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(LblTotalVendaN, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        PnlInfoProduto.setBackground(new java.awt.Color(255, 204, 102));

        LblNomeProduto.setBackground(new java.awt.Color(255, 255, 255));
        LblNomeProduto.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        LblNomeProduto.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblNomeProduto.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        LblNomeProduto.setOpaque(true);

        LblQnt.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        LblQnt.setText("Quantidade");

        LblPrecoUnitario.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        LblPrecoUnitario.setText("Preço Unitário");

        LblTotalItem.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        LblTotalItem.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        LblTotalItem.setText("Total Item");

        LblTotalItemN.setFont(new java.awt.Font("Tahoma", 1, 30)); // NOI18N
        LblTotalItemN.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblTotalItemN.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 1, true));
        LblTotalItemN.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        LblTotalItemN.setMaximumSize(new java.awt.Dimension(200, 44));

        LblPrecoUnitarioN.setFont(new java.awt.Font("Tahoma", 1, 30)); // NOI18N
        LblPrecoUnitarioN.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblPrecoUnitarioN.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 1, true));
        LblPrecoUnitarioN.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        LblPrecoUnitarioN.setMaximumSize(new java.awt.Dimension(200, 44));

        TxtQuantidade.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        TxtQuantidade.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TxtQuantidade.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TxtQuantidadeActionPerformed(evt);
            }
        });

        BtnConfirmaProduto.setIcon(new javax.swing.ImageIcon(getClass().getResource("/planet_sports/Icons/Icone.OK.png"))); // NOI18N
        BtnConfirmaProduto.setText("Confirmar Produto");
        BtnConfirmaProduto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnConfirmaProdutoActionPerformed(evt);
            }
        });

        BtnCancelarItem.setIcon(new javax.swing.ImageIcon(getClass().getResource("/planet_sports/Icons/Icone.Limpar.png"))); // NOI18N
        BtnCancelarItem.setText("Limpar Produto");
        BtnCancelarItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnCancelarItemActionPerformed(evt);
            }
        });

        BtnExcluirItem.setIcon(new javax.swing.ImageIcon(getClass().getResource("/planet_sports/Icons/Icone.Deletar.png"))); // NOI18N
        BtnExcluirItem.setText("Excluir Item");
        BtnExcluirItem.setPreferredSize(new java.awt.Dimension(141, 33));
        BtnExcluirItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnExcluirItemActionPerformed(evt);
            }
        });

        BtnFecharVenda.setIcon(new javax.swing.ImageIcon(getClass().getResource("/planet_sports/Icons/carrinho-de-compras.png"))); // NOI18N
        BtnFecharVenda.setText("Confirmar Venda");
        BtnFecharVenda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnFecharVendaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout PnlInfoProdutoLayout = new javax.swing.GroupLayout(PnlInfoProduto);
        PnlInfoProduto.setLayout(PnlInfoProdutoLayout);
        PnlInfoProdutoLayout.setHorizontalGroup(
            PnlInfoProdutoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PnlInfoProdutoLayout.createSequentialGroup()
                .addGroup(PnlInfoProdutoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(LblQnt, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(TxtQuantidade))
                .addGap(60, 60, 60)
                .addGroup(PnlInfoProdutoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(LblPrecoUnitarioN, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(LblPrecoUnitario, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(60, 60, 60)
                .addGroup(PnlInfoProdutoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(LblTotalItemN, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(PnlInfoProdutoLayout.createSequentialGroup()
                        .addComponent(LblTotalItem, javax.swing.GroupLayout.PREFERRED_SIZE, 116, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))))
            .addComponent(LblNomeProduto, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(PnlInfoProdutoLayout.createSequentialGroup()
                .addComponent(BtnConfirmaProduto)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(BtnCancelarItem)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(BtnFecharVenda)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(BtnExcluirItem, javax.swing.GroupLayout.PREFERRED_SIZE, 141, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        PnlInfoProdutoLayout.setVerticalGroup(
            PnlInfoProdutoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, PnlInfoProdutoLayout.createSequentialGroup()
                .addGroup(PnlInfoProdutoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(LblQnt)
                    .addComponent(LblPrecoUnitario)
                    .addComponent(LblTotalItem))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(PnlInfoProdutoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(PnlInfoProdutoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(LblTotalItemN, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(LblPrecoUnitarioN, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(TxtQuantidade))
                .addGap(18, 18, 18)
                .addComponent(LblNomeProduto, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(PnlInfoProdutoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(BtnConfirmaProduto, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(BtnCancelarItem, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(BtnExcluirItem, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(BtnFecharVenda, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        javax.swing.GroupLayout VendaBackgroundLayout = new javax.swing.GroupLayout(VendaBackground);
        VendaBackground.setLayout(VendaBackgroundLayout);
        VendaBackgroundLayout.setHorizontalGroup(
            VendaBackgroundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(VendaBackgroundLayout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addGroup(VendaBackgroundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(PnlInfoProduto, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, VendaBackgroundLayout.createSequentialGroup()
                        .addComponent(PnlCodPesquisa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(PnlNota, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18))
        );
        VendaBackgroundLayout.setVerticalGroup(
            VendaBackgroundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(VendaBackgroundLayout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addGroup(VendaBackgroundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(PnlNota, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(PnlCodPesquisa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(PnlInfoProduto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(VendaBackground, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(VendaBackground, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

  private void TxtCodigoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TxtCodigoActionPerformed
      BuscarProduto(Produto);
  }//GEN-LAST:event_TxtCodigoActionPerformed

  private void BtnConfirmaProdutoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnConfirmaProdutoActionPerformed
        
      
        if(Produto.getProduto() != null){
            if (!Produto.getProduto().equals("")){
                String DadosCatalogo[] = {Produto.getProduto(),TxtQuantidade.getText(),"R$" + String.valueOf(Produto.getPreco()), LblTotalItemN.getText()};
                DefaultTableModel ModeloTabela = (DefaultTableModel)CarrinhoCompra.getModel();

                ModeloTabela.addRow(DadosCatalogo);
                TotalCompra = TotalCompra + Float.valueOf(LblTotalItemN.getText().substring(2));
                LblTotalVendaN.setText("R$" + String.valueOf(TotalCompra));
                
                LimparItem();
                Produto.Limpar(); 
            }else{
                JOptionPane.showMessageDialog(null, "Selecione um produto");
            }
        }else{
            JOptionPane.showMessageDialog(null, "Selecione um produto");
        }
      
      
        
  }//GEN-LAST:event_BtnConfirmaProdutoActionPerformed

  private void BtnCancelarItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnCancelarItemActionPerformed
      LimparItem();
  }//GEN-LAST:event_BtnCancelarItemActionPerformed

    private void BtnVoltarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnVoltarActionPerformed
        MainScreen Menu = new MainScreen(User, Permissao);
        Menu.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_BtnVoltarActionPerformed

    private void BtnPesquisarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnPesquisarActionPerformed
        BuscarProduto(Produto);
    }//GEN-LAST:event_BtnPesquisarActionPerformed

    private void TxtQuantidadeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TxtQuantidadeActionPerformed
        
        int Qnt = Integer.valueOf(TxtQuantidade.getText());
        
        LblTotalItemN.setText("R$" + String.valueOf(Qnt * Produto.getPreco()));
        
    }//GEN-LAST:event_TxtQuantidadeActionPerformed

    private void BtnCancelarVendaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnCancelarVendaActionPerformed
        int Linhas = CarrinhoCompra.getRowCount();
        
        Object Opcoes[] = {"Sim", "Não"};
        
        if (Linhas > 0){
            if(PopupCancelaVenda(Opcoes)){
                LimparTela();
            }
        }else{
            JOptionPane.showMessageDialog(null, "Não há venda cadastrada");
        }
        
        
    }//GEN-LAST:event_BtnCancelarVendaActionPerformed

    private void BtnExcluirItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnExcluirItemActionPerformed
       
        int Linhas = CarrinhoCompra.getRowCount();
        
        Object Opcoes[] = {"Sim", "Não"};
        
        if (Linhas > 0){
            if(PopupOpcao(Opcoes)){
                ExcluirItem();
            }
        }else{
            JOptionPane.showMessageDialog(null, "Não há produtos para deletar");
        }
        

    }//GEN-LAST:event_BtnExcluirItemActionPerformed

    private void BtnFecharVendaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnFecharVendaActionPerformed
        int Linhas = CarrinhoCompra.getRowCount();
        
        if(Linhas > 0){
            TelaPagamento Pagamento = new TelaPagamento(this);
            Pagamento.setVisible(true);
            this.setVisible(false);
        }else{
            JOptionPane.showMessageDialog(null, "Não há produtos no carrinho");
        }
        
    }//GEN-LAST:event_BtnFecharVendaActionPerformed
    
    private boolean PopupOpcao(Object Opcoes[]){
        int Escolha;
        
        Escolha = PopUp.showOptionDialog(this, "Deseja deletar o produto?", "Prestes a deletar um produto!",
        PopUp.YES_NO_OPTION, PopUp.QUESTION_MESSAGE,null,Opcoes,
        Opcoes[0]);
        
        return Escolha == 0;
    }
    
    public String getTotalVenda(){
        return LblTotalVendaN.getText();
    }
    
    private boolean PopupCancelaVenda(Object Opcoes[]){
        int Escolha;
        
        Escolha = PopUp.showOptionDialog(this, "Deseja cancelar a venda?", "Prestes a cancelar a venda!",
        PopUp.YES_NO_OPTION, PopUp.QUESTION_MESSAGE,null,Opcoes,
        Opcoes[0]);
        
        return Escolha == 0;
    }
    
    private void ExcluirItem(){
        
        int Linha = CarrinhoCompra.getSelectedRow();
        DefaultTableModel ModeloTabela = (DefaultTableModel) CarrinhoCompra.getModel();
        
        if(Linha >= 0){
            float ValorTProduto = Float.valueOf(CarrinhoCompra.getModel().getValueAt(Linha, 3).toString().substring(2));
            TotalCompra = TotalCompra - ValorTProduto;
            LblTotalVendaN.setText("R$" + String.valueOf(TotalCompra));
            ModeloTabela.removeRow(Linha);  
        }else{
            JOptionPane.showMessageDialog(this, "Selecione um item do catálogo");
        }
        
        CarrinhoCompra.clearSelection();
    }
    
    private void LimparItem(){
        TxtCodigo.setText("");
        TxtQuantidade.setText("");
        LblPrecoUnitarioN.setText("");
        LblNomeProduto.setText("");
        LblTotalItemN.setText("");
    }
    
    private void LimparTela(){
        LimparItem();
        Produto.Limpar();
        TotalCompra = 0;
        LblTotalVendaN.setText("");
        
        DefaultTableModel ModeloTabela = (DefaultTableModel) CarrinhoCompra.getModel();
        ModeloTabela.setRowCount(0);
    }
    
    private void BuscarProduto(Produto Produto){
        
        this.conectar.conectaBanco();
        
        try {
            this.conectar.executarSQL(
                   "SELECT * FROM Produto WHERE idProduto = " + TxtCodigo.getText() + ";"
            );
            
            while(this.conectar.getResultSet().next()){
                Produto.setCodigo(this.conectar.getResultSet().getInt(1));
                Produto.setProduto(this.conectar.getResultSet().getString(2));
                Produto.setDescricao(this.conectar.getResultSet().getString(3));
                Produto.setPreco(this.conectar.getResultSet().getFloat(4));
                Produto.setCusto(this.conectar.getResultSet().getFloat(5));
                Produto.setEsporte(this.conectar.getResultSet().getString(6));
            }
           
            if(Produto.getProduto() == null){
                JOptionPane.showMessageDialog(null, "Produto não encontrado!");
            }else{
                if(Produto.getProduto().equals("")){
                    JOptionPane.showMessageDialog(null, "Produto não encontrado!");
                }else{
                LimparItem();
                    TxtQuantidade.setText("1");
                    LblPrecoUnitarioN.setText("R$" + String.valueOf(Produto.getPreco()));
                    LblTotalItemN.setText("R$" + String.valueOf(Produto.getPreco()));
                    LblNomeProduto.setText(Produto.getProduto());
                }
            }
            
            
           
        } catch (Exception e) {            
            System.out.println("Erro ao consultar produto " +  e.getMessage());
            JOptionPane.showMessageDialog(null, "Erro ao buscar produto");
            
        }finally{
            this.conectar.fechaBanco();   
        }               
    }
  
  public static void main(String args[]) {
    /* Set the Nimbus look and feel */
    //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
    /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
     */
    try {
      for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
        if ("Nimbus".equals(info.getName())) {
          javax.swing.UIManager.setLookAndFeel(info.getClassName());
          break;
        }
      }
    } catch (ClassNotFoundException ex) {
      java.util.logging.Logger.getLogger(TelaVenda.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
    } catch (InstantiationException ex) {
      java.util.logging.Logger.getLogger(TelaVenda.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
    } catch (IllegalAccessException ex) {
      java.util.logging.Logger.getLogger(TelaVenda.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
    } catch (javax.swing.UnsupportedLookAndFeelException ex) {
      java.util.logging.Logger.getLogger(TelaVenda.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
    }
    //</editor-fold>

    /* Create and display the form */
    java.awt.EventQueue.invokeLater(new Runnable() {
      public void run() {
        new TelaVenda().setVisible(true);
      }
    });
  }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton BtnCancelarItem;
    private javax.swing.JButton BtnCancelarVenda;
    private javax.swing.JButton BtnConfirmaProduto;
    private javax.swing.JButton BtnExcluirItem;
    private javax.swing.JButton BtnFecharVenda;
    private javax.swing.JButton BtnPesquisar;
    private javax.swing.JButton BtnVoltar;
    private javax.swing.JTable CarrinhoCompra;
    private javax.swing.JLabel IconPlanetSports;
    private javax.swing.JLabel LblCodigo;
    private javax.swing.JLabel LblNomeProduto;
    private javax.swing.JLabel LblPrecoUnitario;
    private javax.swing.JLabel LblPrecoUnitarioN;
    private javax.swing.JLabel LblQnt;
    private javax.swing.JLabel LblTotalItem;
    private javax.swing.JLabel LblTotalItemN;
    private javax.swing.JLabel LblTotalVenda;
    private javax.swing.JLabel LblTotalVendaN;
    private javax.swing.JPanel PnlCodPesquisa;
    private javax.swing.JPanel PnlInfoProduto;
    private javax.swing.JPanel PnlNota;
    private javax.swing.JScrollPane ScrollVenda;
    public javax.swing.JTextField TxtCodigo;
    private javax.swing.JTextField TxtQuantidade;
    private javax.swing.JPanel VendaBackground;
    // End of variables declaration//GEN-END:variables
}
