/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package planet_sports;

import Objetos.Cliente;
import javax.swing.JOptionPane;
import Conexao.MySQL;
import Objetos.Endereco;


public class TelaCadastroCliente extends javax.swing.JFrame {

    MySQL conectar = new MySQL();
    Cliente NovoCliente = new Cliente();
    Endereco NovoEndereco = new Endereco();
    String User;
    String Permissao;
    
  public TelaCadastroCliente(String User, String Permissao) {
    initComponents();
    this.User = User;
    this.Permissao = Permissao;
  }
  
  public TelaCadastroCliente() {
    initComponents();
  }

  @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        Background = new javax.swing.JPanel();
        PnlTitulo = new javax.swing.JPanel();
        LblCadastroCliente = new javax.swing.JLabel();
        PnlInfoCliente = new javax.swing.JPanel();
        PnlInfoPessoa = new javax.swing.JPanel();
        LblNomeComepleto = new javax.swing.JLabel();
        TxtNomeCompleto = new javax.swing.JTextField();
        LblDocumento = new javax.swing.JLabel();
        TxtDocumento = new javax.swing.JTextField();
        LblDataNascimento = new javax.swing.JLabel();
        LblTelefone = new javax.swing.JLabel();
        TxtTelefone = new javax.swing.JTextField();
        TxtDataNascimento = new javax.swing.JFormattedTextField();
        PnlEndereco = new javax.swing.JPanel();
        LblEndereco = new javax.swing.JLabel();
        TxtEndereco = new javax.swing.JTextField();
        LblNumero = new javax.swing.JLabel();
        TxtNumero = new javax.swing.JTextField();
        LblBairro = new javax.swing.JLabel();
        TxtBairro = new javax.swing.JTextField();
        LblCidade = new javax.swing.JLabel();
        TxtCidade = new javax.swing.JTextField();
        LblEstado = new javax.swing.JLabel();
        CBoxEstado = new javax.swing.JComboBox<>();
        PnlBotoes = new javax.swing.JPanel();
        BtnLimpar = new javax.swing.JButton();
        BtnCadastrar = new javax.swing.JButton();
        BtnVoltar = new javax.swing.JButton();
        BntBuscar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        Background.setBackground(new java.awt.Color(255, 204, 102));
        Background.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        PnlTitulo.setBackground(new java.awt.Color(255, 204, 102));

        LblCadastroCliente.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        LblCadastroCliente.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblCadastroCliente.setText("CADASTRO DE CLIENTE");
        LblCadastroCliente.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        javax.swing.GroupLayout PnlTituloLayout = new javax.swing.GroupLayout(PnlTitulo);
        PnlTitulo.setLayout(PnlTituloLayout);
        PnlTituloLayout.setHorizontalGroup(
            PnlTituloLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PnlTituloLayout.createSequentialGroup()
                .addGap(97, 97, 97)
                .addComponent(LblCadastroCliente, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(96, 96, 96))
        );
        PnlTituloLayout.setVerticalGroup(
            PnlTituloLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PnlTituloLayout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(LblCadastroCliente)
                .addGap(0, 0, 0))
        );

        PnlInfoCliente.setBackground(new java.awt.Color(255, 204, 102));

        PnlInfoPessoa.setBackground(new java.awt.Color(255, 204, 102));

        LblNomeComepleto.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        LblNomeComepleto.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        LblNomeComepleto.setText("Nome");

        TxtNomeCompleto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TxtNomeCompletoActionPerformed(evt);
            }
        });

        LblDocumento.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        LblDocumento.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        LblDocumento.setText("RG/CPF");
        LblDocumento.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);

        TxtDocumento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TxtDocumentoActionPerformed(evt);
            }
        });

        LblDataNascimento.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        LblDataNascimento.setText("Data de Nascimento");
        LblDataNascimento.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);

        LblTelefone.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        LblTelefone.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        LblTelefone.setText("Telefone");
        LblTelefone.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);

        TxtTelefone.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TxtTelefoneActionPerformed(evt);
            }
        });

        try {
            TxtDataNascimento.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        javax.swing.GroupLayout PnlInfoPessoaLayout = new javax.swing.GroupLayout(PnlInfoPessoa);
        PnlInfoPessoa.setLayout(PnlInfoPessoaLayout);
        PnlInfoPessoaLayout.setHorizontalGroup(
            PnlInfoPessoaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PnlInfoPessoaLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(PnlInfoPessoaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(LblTelefone, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(LblDocumento, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(LblNomeComepleto, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(PnlInfoPessoaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(TxtNomeCompleto)
                    .addGroup(PnlInfoPessoaLayout.createSequentialGroup()
                        .addGroup(PnlInfoPessoaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(TxtTelefone, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(TxtDocumento, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(95, 95, 95)
                        .addComponent(LblDataNascimento)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(TxtDataNascimento, javax.swing.GroupLayout.DEFAULT_SIZE, 84, Short.MAX_VALUE)))
                .addContainerGap())
        );
        PnlInfoPessoaLayout.setVerticalGroup(
            PnlInfoPessoaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PnlInfoPessoaLayout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addGroup(PnlInfoPessoaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(LblNomeComepleto)
                    .addComponent(TxtNomeCompleto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(PnlInfoPessoaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(LblDocumento)
                    .addComponent(TxtDocumento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(LblDataNascimento)
                    .addComponent(TxtDataNascimento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(PnlInfoPessoaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(LblTelefone)
                    .addComponent(TxtTelefone, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10))
        );

        PnlEndereco.setBackground(new java.awt.Color(255, 204, 102));

        LblEndereco.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        LblEndereco.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        LblEndereco.setText("Endereço");

        TxtEndereco.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TxtEnderecoActionPerformed(evt);
            }
        });

        LblNumero.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        LblNumero.setText("n°");

        TxtNumero.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TxtNumero.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TxtNumeroActionPerformed(evt);
            }
        });

        LblBairro.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        LblBairro.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        LblBairro.setText("Bairro");

        TxtBairro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TxtBairroActionPerformed(evt);
            }
        });

        LblCidade.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        LblCidade.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        LblCidade.setText("Cidade");

        TxtCidade.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TxtCidadeActionPerformed(evt);
            }
        });

        LblEstado.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        LblEstado.setText("UF");

        CBoxEstado.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "", "AC", "AL", "AP", "AM", "BA", "CE", "DF", "ES", "GO", "MA", "MT", "MS", "MG", "PA", "PB", "PR", "PE", "PI", "RR", "RO", "RJ", "RN", "RS", "SC", "SP", "SE", "TO" }));

        javax.swing.GroupLayout PnlEnderecoLayout = new javax.swing.GroupLayout(PnlEndereco);
        PnlEndereco.setLayout(PnlEnderecoLayout);
        PnlEnderecoLayout.setHorizontalGroup(
            PnlEnderecoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PnlEnderecoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(PnlEnderecoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(LblEndereco)
                    .addComponent(LblBairro, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(10, 10, 10)
                .addGroup(PnlEnderecoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(PnlEnderecoLayout.createSequentialGroup()
                        .addComponent(TxtBairro, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(LblCidade)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(TxtCidade, javax.swing.GroupLayout.DEFAULT_SIZE, 155, Short.MAX_VALUE))
                    .addComponent(TxtEndereco))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(PnlEnderecoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(LblNumero)
                    .addComponent(LblEstado))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(PnlEnderecoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(CBoxEstado, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(TxtNumero))
                .addContainerGap())
        );
        PnlEnderecoLayout.setVerticalGroup(
            PnlEnderecoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PnlEnderecoLayout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addGroup(PnlEnderecoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(LblEndereco)
                    .addComponent(TxtEndereco, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(LblNumero)
                    .addComponent(TxtNumero, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(PnlEnderecoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(LblBairro)
                    .addComponent(TxtBairro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(LblCidade)
                    .addComponent(TxtCidade, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(LblEstado)
                    .addComponent(CBoxEstado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10))
        );

        PnlBotoes.setBackground(new java.awt.Color(255, 204, 102));

        BtnLimpar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/planet_sports/Icons/Icone.Limpar.png"))); // NOI18N
        BtnLimpar.setText("Limpar");
        BtnLimpar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnLimparActionPerformed(evt);
            }
        });

        BtnCadastrar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/planet_sports/Icons/Icone.Salvar.png"))); // NOI18N
        BtnCadastrar.setText("Cadastrar");
        BtnCadastrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnCadastrarActionPerformed(evt);
            }
        });

        BtnVoltar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/planet_sports/Icons/Icone.Voltar.png"))); // NOI18N
        BtnVoltar.setText("Voltar");
        BtnVoltar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnVoltarActionPerformed(evt);
            }
        });

        BntBuscar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/planet_sports/Icons/Icone.Lupa2.png"))); // NOI18N
        BntBuscar.setText("Consultar");
        BntBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BntBuscarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout PnlBotoesLayout = new javax.swing.GroupLayout(PnlBotoes);
        PnlBotoes.setLayout(PnlBotoesLayout);
        PnlBotoesLayout.setHorizontalGroup(
            PnlBotoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PnlBotoesLayout.createSequentialGroup()
                .addGap(77, 77, 77)
                .addGroup(PnlBotoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(BtnCadastrar, javax.swing.GroupLayout.PREFERRED_SIZE, 114, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(BtnLimpar, javax.swing.GroupLayout.PREFERRED_SIZE, 114, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(128, 128, 128)
                .addGroup(PnlBotoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(BntBuscar, javax.swing.GroupLayout.DEFAULT_SIZE, 114, Short.MAX_VALUE)
                    .addComponent(BtnVoltar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(73, Short.MAX_VALUE))
        );
        PnlBotoesLayout.setVerticalGroup(
            PnlBotoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PnlBotoesLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(PnlBotoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(BtnCadastrar)
                    .addComponent(BntBuscar))
                .addGap(28, 28, 28)
                .addGroup(PnlBotoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(BtnLimpar)
                    .addComponent(BtnVoltar))
                .addContainerGap())
        );

        javax.swing.GroupLayout PnlInfoClienteLayout = new javax.swing.GroupLayout(PnlInfoCliente);
        PnlInfoCliente.setLayout(PnlInfoClienteLayout);
        PnlInfoClienteLayout.setHorizontalGroup(
            PnlInfoClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(PnlBotoes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addGroup(PnlInfoClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                .addComponent(PnlEndereco, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(PnlInfoPessoa, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        PnlInfoClienteLayout.setVerticalGroup(
            PnlInfoClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PnlInfoClienteLayout.createSequentialGroup()
                .addComponent(PnlInfoPessoa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(PnlEndereco, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(PnlBotoes, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout BackgroundLayout = new javax.swing.GroupLayout(Background);
        Background.setLayout(BackgroundLayout);
        BackgroundLayout.setHorizontalGroup(
            BackgroundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(BackgroundLayout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addGroup(BackgroundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(PnlTitulo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(PnlInfoCliente, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        BackgroundLayout.setVerticalGroup(
            BackgroundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, BackgroundLayout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addComponent(PnlTitulo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(PnlInfoCliente, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(Background, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(Background, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

  

  private void TxtNomeCompletoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TxtNomeCompletoActionPerformed
    // TODO add your handling code here:
  }//GEN-LAST:event_TxtNomeCompletoActionPerformed

  private void TxtDocumentoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TxtDocumentoActionPerformed
    // TODO add your handling code here:
  }//GEN-LAST:event_TxtDocumentoActionPerformed

  private void TxtEnderecoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TxtEnderecoActionPerformed
    // TODO add your handling code here:
  }//GEN-LAST:event_TxtEnderecoActionPerformed

  private void TxtTelefoneActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TxtTelefoneActionPerformed
    // TODO add your handling code here:
  }//GEN-LAST:event_TxtTelefoneActionPerformed

  private void TxtNumeroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TxtNumeroActionPerformed
    // TODO add your handling code here:
  }//GEN-LAST:event_TxtNumeroActionPerformed

  private void TxtBairroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TxtBairroActionPerformed
    // TODO add your handling code here:
  }//GEN-LAST:event_TxtBairroActionPerformed

  private void TxtCidadeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TxtCidadeActionPerformed
    // TODO add your handling code here:
  }//GEN-LAST:event_TxtCidadeActionPerformed

  private void BtnCadastrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnCadastrarActionPerformed
    
    if(isInfoOK()){
        CadastrarCliente(NovoCliente);
    }
  
  }//GEN-LAST:event_BtnCadastrarActionPerformed

  private void BtnLimparActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnLimparActionPerformed
    LimparFormulario();
  }//GEN-LAST:event_BtnLimparActionPerformed

  private void BtnVoltarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnVoltarActionPerformed
      MainScreen MenuPrincipal = new MainScreen(User,Permissao);
      MenuPrincipal.setVisible(true);
      this.dispose();
  }//GEN-LAST:event_BtnVoltarActionPerformed

    private void BntBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BntBuscarActionPerformed
        TelaConsultaClientes ConsultaCliente = new TelaConsultaClientes();
        ConsultaCliente.setVisible(true);
        this.dispose(); 
    }//GEN-LAST:event_BntBuscarActionPerformed
  
  private void CadastrarCliente(Cliente NovoCliente){
        
        this.conectar.conectaBanco();
        
        NovoCliente.setNomeCompleto(TxtNomeCompleto.getText());
        NovoCliente.setDataNascimento(TxtDataNascimento.getText());
        NovoCliente.setDocumento(TxtDocumento.getText());
        NovoCliente.setTelefone(TxtTelefone.getText());
        NovoCliente.setDocumento(TxtDocumento.getText());
        NovoEndereco.setRua(TxtEndereco.getText());
        NovoEndereco.setNumero(Integer.valueOf(TxtNumero.getText()));
        NovoEndereco.setCidade(TxtCidade.getText());
        NovoEndereco.setBairro(TxtBairro.getText());
        NovoEndereco.setUF(String.valueOf(CBoxEstado.getSelectedItem()));
        
        int idEndereco = EnderecoExiste(NovoEndereco);
        
        
        if(idEndereco >= 0){
            while (idEndereco == 0){
                CadastraEndereco(NovoEndereco);
                idEndereco = EnderecoExiste(NovoEndereco);
            }
            this.conectar.conectaBanco();
            try{
                this.conectar.insertSQL("insert into Cliente values (null  ," +
                                    "'" + NovoCliente.getNomeCompleto()  + "'," +
                                    "'" + NovoCliente.getDocumento()     + "'," +
                                    "'" + NovoCliente.getDataNascimento()+ "'," +
                                    "'" + NovoCliente.getTelefone()      + "'," +
                                    "'" + idEndereco                     + "');");
            }catch(Exception e){
                this.conectar.fechaBanco();
                System.out.println("Erro ao cadastrar Cliente " +  e.getMessage());
                JOptionPane.showMessageDialog(null, "Erro ao cadastrar Cliente"); 
            }finally{            
                this.conectar.fechaBanco();
                JOptionPane.showMessageDialog(null, "Cliente cadastrado com sucesso");
                LimparFormulario();
        }
    }
  }
  
  private void CadastraEndereco(Endereco NovoEndereco){
    this.conectar.conectaBanco();
    
    try{
        this.conectar.insertSQL("insert into Endereco values (" +
                                "null"                        + "," +
                                "'" + NovoEndereco.getRua()    + "'," +
                                "'" + NovoEndereco.getNumero() + "'," +
                                "'" + NovoEndereco.getBairro() + "'," +
                                "'" + NovoEndereco.getCidade() + "'," +
                                "'" + NovoEndereco.getUF()     + "');");
    }catch(Exception e){
        System.out.println("Erro ao cadastrar Endereco " +  e.getMessage());
        JOptionPane.showMessageDialog(null, "Erro ao cadastrar Endereco");
    }
  }
  
  private boolean isInfoOK(){
        
        String Mensagem = "";
        String NomeCompleto = TxtNomeCompleto.getText();
        String DataNascimento = TxtDataNascimento.getText();
        String Documento = TxtDocumento.getText();
        String Endereco = TxtEndereco.getText();
        String Bairro = TxtBairro.getText();
        String Cidade = TxtCidade.getText();
        String Telefone = TxtTelefone.getText();
        
        int TamanhoDocumento  = Documento.length();
        int TamanhoTelefone   = Telefone.length();
        
        if( NomeCompleto.equals("")     ||
            Documento.equals("")        ||
            DataNascimento.equals("")   ||
            Endereco.equals("")         ||
            Bairro.equals("")           ||
            Cidade.equals("")){
            
            Mensagem = Mensagem + "Preencha todos os campos.\n";            
        }
        
        if (TamanhoDocumento < 8 || TamanhoDocumento > 11) {
            Mensagem = Mensagem + "Documento inválido.\n";
            TxtDocumento.setText("");
        }else{
            if (ClienteExiste(Documento)) {
                Mensagem = Mensagem + "Cliente já cadastrado.\n";
                TxtNomeCompleto.setText("");
                LimparFormulario();
            }
        }
        
        if (!TxtNumero.getText().equals("")){
            if(!isStringInt(TxtNumero.getText())){
                Mensagem = Mensagem + "Numero de endereco invalido.\n";
                TxtNumero.setText("");
            }else{
                if(Integer.valueOf(TxtNumero.getText()) < 0){
                    Mensagem = Mensagem + "Numero de endereco invalido.\n";
                    TxtNumero.setText(""); 
                }                
            }
        }
        
        if(!Telefone.equals("")){
            if (TamanhoTelefone < 8 || TamanhoTelefone > 9){
                Mensagem = Mensagem + "Numero de telefone invalido.\n";
                TxtTelefone.setText("");
            }
        }
        
        if (!Mensagem.equals("")){
            JOptionPane.showMessageDialog(this, Mensagem);
            return false;
        }else {
            return true;
        }
    }
  
  private boolean ClienteExiste(String DocumentoDigitado){
      this.conectar.conectaBanco();
      String DocEncontrado  = "";
        
        try {
           this.conectar.executarSQL("select * from Cliente where Documento = " + "'" + DocumentoDigitado + "'" + ";"); //SQL para buscar no Banco
           
           while(this.conectar.getResultSet().next()){
               DocEncontrado = this.conectar.getResultSet().getString(3);// 1 é o ID, 2 o nome, 3 doc
           }
           
           if(DocEncontrado.equals(DocumentoDigitado)){ // Se o user for encontrado, retorna verdadeiro
               this.conectar.fechaBanco();
               return true;
           } else{ //Se nao, retorna falso
               this.conectar.fechaBanco();
               return false;
           }
           
        }catch(Exception e){
            System.out.println("Erro ao verificar Cliente " +  e.getMessage());
            JOptionPane.showMessageDialog(null, "Erro ao verificar Cliente");
            this.conectar.fechaBanco();
            return true;
        }
   }
  
  private int EnderecoExiste(Endereco EnderecoDigitado){
      this.conectar.conectaBanco();
      String RuaEncontrada  = "";
      int idEncontrado = 0;
      int NumeroEncontrado = -1;
      
        
        try {
           this.conectar.executarSQL("select * from Endereco where Rua = " + "'" + EnderecoDigitado.getRua() + "'" 
                  + "and Numero = " + "'" + EnderecoDigitado.getNumero() + "'" + ";"); //SQL para buscar no Banco
                      
           
           while(this.conectar.getResultSet().next()){
               idEncontrado  =  this.conectar.getResultSet().getInt(1);
               RuaEncontrada =  this.conectar.getResultSet().getString(2);
               NumeroEncontrado  =  this.conectar.getResultSet().getInt(3);
           }
           
           if(RuaEncontrada.equals(EnderecoDigitado.getRua())){
               if (EnderecoDigitado.getNumero() == NumeroEncontrado){
                   this.conectar.fechaBanco();
                   return idEncontrado;
               } else{
                    this.conectar.fechaBanco();
                    return 0;
               }
           } else{
               this.conectar.fechaBanco();
               return 0;
           }
           
        }catch(Exception e){
            System.out.println("Erro ao verificar Endereco " +  e.getMessage());
            JOptionPane.showMessageDialog(null, "Erro ao verificar Endereco");
            this.conectar.fechaBanco();
            return -1;
        }
   }

  private void LimparFormulario(){
        TxtNomeCompleto.setText("");
        TxtDataNascimento.setText("");
        TxtDocumento.setText("");
        TxtEndereco.setText("");
        TxtTelefone.setText("");
        TxtNumero.setText("");
        TxtBairro.setText("");
        TxtCidade.setText("");
        CBoxEstado.setSelectedIndex(0); 
    }
  
  public boolean isStringInt(String s){
      
            try {
                Integer.parseInt(s);
                return true;
            } catch (NumberFormatException ex) {
                return false;
            }
        } 
  
  public static void main(String args[]) {

    //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
    /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
     */
    try {
      for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
        if ("Nimbus".equals(info.getName())) {
          javax.swing.UIManager.setLookAndFeel(info.getClassName());
          break;
        }
      }
    } catch (ClassNotFoundException ex) {
      java.util.logging.Logger.getLogger(TelaCadastroCliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
    } catch (InstantiationException ex) {
      java.util.logging.Logger.getLogger(TelaCadastroCliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
    } catch (IllegalAccessException ex) {
      java.util.logging.Logger.getLogger(TelaCadastroCliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
    } catch (javax.swing.UnsupportedLookAndFeelException ex) {
      java.util.logging.Logger.getLogger(TelaCadastroCliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
    }
    //</editor-fold>


    java.awt.EventQueue.invokeLater(new Runnable() {
      public void run() {
        new TelaCadastroCliente().setVisible(true);
      }
    });
  }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel Background;
    private javax.swing.JButton BntBuscar;
    private javax.swing.JButton BtnCadastrar;
    private javax.swing.JButton BtnLimpar;
    private javax.swing.JButton BtnVoltar;
    private javax.swing.JComboBox<String> CBoxEstado;
    private javax.swing.JLabel LblBairro;
    private javax.swing.JLabel LblCadastroCliente;
    private javax.swing.JLabel LblCidade;
    private javax.swing.JLabel LblDataNascimento;
    private javax.swing.JLabel LblDocumento;
    private javax.swing.JLabel LblEndereco;
    private javax.swing.JLabel LblEstado;
    private javax.swing.JLabel LblNomeComepleto;
    private javax.swing.JLabel LblNumero;
    private javax.swing.JLabel LblTelefone;
    private javax.swing.JPanel PnlBotoes;
    private javax.swing.JPanel PnlEndereco;
    private javax.swing.JPanel PnlInfoCliente;
    private javax.swing.JPanel PnlInfoPessoa;
    private javax.swing.JPanel PnlTitulo;
    private javax.swing.JTextField TxtBairro;
    private javax.swing.JTextField TxtCidade;
    private javax.swing.JFormattedTextField TxtDataNascimento;
    private javax.swing.JTextField TxtDocumento;
    private javax.swing.JTextField TxtEndereco;
    private javax.swing.JTextField TxtNomeCompleto;
    private javax.swing.JTextField TxtNumero;
    private javax.swing.JTextField TxtTelefone;
    // End of variables declaration//GEN-END:variables
}
