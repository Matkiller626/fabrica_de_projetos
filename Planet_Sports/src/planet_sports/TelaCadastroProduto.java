
package planet_sports;

import Conexao.MySQL;
import Objetos.Produto;
import javax.swing.JOptionPane;


public class TelaCadastroProduto extends javax.swing.JFrame {
    
    MySQL conectar = new MySQL(); //acessar os métodos de conexao com o banco
    Produto NovoProduto = new Produto();
    String User;
    String Permissao;
    
  public TelaCadastroProduto(String User, String Permissao) {
    initComponents();
    this.User = User;
    this.Permissao = Permissao;
  }
  
  public TelaCadastroProduto() {
    initComponents();
  }

  @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        LblTitulo = new javax.swing.JLabel();
        LblNome = new javax.swing.JLabel();
        LblDescricao = new javax.swing.JLabel();
        LblPreco = new javax.swing.JLabel();
        LblCusto = new javax.swing.JLabel();
        LblEsporte = new javax.swing.JLabel();
        TxtNome = new javax.swing.JTextField();
        ScrollPanelDescricao = new javax.swing.JScrollPane();
        TxtDescricao = new javax.swing.JTextArea();
        TxtPreco = new javax.swing.JTextField();
        TxtCusto = new javax.swing.JTextField();
        CBoxEsporte = new javax.swing.JComboBox<>();
        Btn_Cadastrar = new javax.swing.JButton();
        Btn_Limpar = new javax.swing.JButton();
        Btn_MenuPrincipal = new javax.swing.JButton();
        BtnConsulta = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(255, 204, 102));
        setResizable(false);

        jPanel1.setBackground(new java.awt.Color(255, 204, 102));
        jPanel1.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jPanel1.setPreferredSize(new java.awt.Dimension(560, 380));

        LblTitulo.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        LblTitulo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblTitulo.setText("CADASTRO DE PRODUTO");

        LblNome.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        LblNome.setText("Nome");

        LblDescricao.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        LblDescricao.setText("Descricao");

        LblPreco.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        LblPreco.setText("Preco");

        LblCusto.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        LblCusto.setText("Custo");

        LblEsporte.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        LblEsporte.setText("Esporte");

        ScrollPanelDescricao.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        ScrollPanelDescricao.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

        TxtDescricao.setColumns(20);
        TxtDescricao.setLineWrap(true);
        TxtDescricao.setRows(5);
        ScrollPanelDescricao.setViewportView(TxtDescricao);

        TxtPreco.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TxtPrecoActionPerformed(evt);
            }
        });

        TxtCusto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TxtCustoActionPerformed(evt);
            }
        });

        CBoxEsporte.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "", "Volei", "Futebol", "Basquete", "Corrida/Caminhada", "Luta/Artes Marciais", "Skate" }));

        Btn_Cadastrar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/planet_sports/Icons/Icone.Salvar.png"))); // NOI18N
        Btn_Cadastrar.setText("Cadastrar");
        Btn_Cadastrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Btn_CadastrarActionPerformed(evt);
            }
        });

        Btn_Limpar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/planet_sports/Icons/Icone.Limpar.png"))); // NOI18N
        Btn_Limpar.setText("Limpar");
        Btn_Limpar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Btn_LimparActionPerformed(evt);
            }
        });

        Btn_MenuPrincipal.setIcon(new javax.swing.ImageIcon(getClass().getResource("/planet_sports/Icons/Icone.voltar.png"))); // NOI18N
        Btn_MenuPrincipal.setText("Voltar");
        Btn_MenuPrincipal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Btn_MenuPrincipalActionPerformed(evt);
            }
        });

        BtnConsulta.setIcon(new javax.swing.ImageIcon(getClass().getResource("/planet_sports/Icons/Icone.Lupa2.png"))); // NOI18N
        BtnConsulta.setText("Consulta");
        BtnConsulta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnConsultaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(LblPreco, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(TxtPreco))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(LblEsporte, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(LblCusto, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(TxtCusto)
                                    .addComponent(CBoxEsporte, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(Btn_Cadastrar, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(Btn_Limpar, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(Btn_MenuPrincipal, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(BtnConsulta, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(LblTitulo, javax.swing.GroupLayout.PREFERRED_SIZE, 404, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(LblNome)
                                    .addComponent(LblDescricao))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(ScrollPanelDescricao, javax.swing.GroupLayout.PREFERRED_SIZE, 335, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(TxtNome, javax.swing.GroupLayout.PREFERRED_SIZE, 335, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addComponent(LblTitulo)
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(TxtNome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(LblNome))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(ScrollPanelDescricao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(LblDescricao))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(LblPreco)
                            .addComponent(TxtPreco, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(31, 31, 31)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(LblCusto)
                            .addComponent(TxtCusto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(31, 31, 31)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(LblEsporte)
                            .addComponent(CBoxEsporte, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(Btn_Cadastrar)
                        .addGap(22, 22, 22)
                        .addComponent(Btn_Limpar)
                        .addGap(22, 22, 22)
                        .addComponent(Btn_MenuPrincipal)
                        .addGap(22, 22, 22)
                        .addComponent(BtnConsulta)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 442, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 402, Short.MAX_VALUE)
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

  private void Btn_MenuPrincipalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Btn_MenuPrincipalActionPerformed
      MainScreen Menu = new MainScreen(User, Permissao);
      Menu.setVisible(true);
      this.dispose();
  }//GEN-LAST:event_Btn_MenuPrincipalActionPerformed

  private void Btn_LimparActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Btn_LimparActionPerformed
      LimparFormulario();
  }//GEN-LAST:event_Btn_LimparActionPerformed

  private void TxtCustoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TxtCustoActionPerformed
   
  }//GEN-LAST:event_TxtCustoActionPerformed

  private void TxtPrecoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TxtPrecoActionPerformed
 
  }//GEN-LAST:event_TxtPrecoActionPerformed

    private void BtnConsultaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnConsultaActionPerformed
        TelaConsultaProduto ConsultaProduto = new TelaConsultaProduto();
        ConsultaProduto.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_BtnConsultaActionPerformed

    private void Btn_CadastrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Btn_CadastrarActionPerformed
        
        if(isInfoOK()){
            CadastrarProduto(NovoProduto);
        }
        
    }//GEN-LAST:event_Btn_CadastrarActionPerformed

  private void CadastrarProduto(Produto NovoProduto){
        
        this.conectar.conectaBanco();
        
        NovoProduto.setProduto(TxtNome.getText());
        NovoProduto.setDescricao(TxtDescricao.getText());
        NovoProduto.setPreco(Float.valueOf(TxtPreco.getText()));
        NovoProduto.setCusto(Float.valueOf(TxtCusto.getText()));
        NovoProduto.setEsporte(String.valueOf(CBoxEsporte.getSelectedItem()));      

        try{
            this.conectar.insertSQL("insert into Produto values (null  ," +
                                "'" + NovoProduto.getProduto()  + "'," +
                                "'" + NovoProduto.getDescricao()+ "'," +
                                "'" + NovoProduto.getPreco()    + "'," +
                                "'" + NovoProduto.getCusto()    + "'," +
                                "'" + NovoProduto.getEsporte()  + "');");
        }catch(Exception e){
            this.conectar.fechaBanco();
            System.out.println("Erro ao cadastrar Produto " +  e.getMessage());
            JOptionPane.showMessageDialog(null, "Erro ao cadastrar Produto"); 
        }finally{            
            this.conectar.fechaBanco();
            JOptionPane.showMessageDialog(null, "Produto cadastrado com sucesso");
            LimparFormulario();
        }
    }
  
  private boolean isInfoOK(){
        
        String Mensagem = "";
        String Produto = TxtNome.getText();
        String Descricao = TxtDescricao.getText();
        String Esporte = String.valueOf(CBoxEsporte.getSelectedItem());
        String Preco = TxtPreco.getText();
        String Custo = TxtCusto.getText();
        
        
        if( Produto.equals("")     ||
            Descricao.equals("")        ||
            Esporte.equals("")   ||
            Preco.equals("")         ||
            Custo.equals("")){
            
            Mensagem = Mensagem + "Preencha todos os campos.\n";            
        }
        
        if (!isStringNumeric(Preco)){
            Mensagem = Mensagem + "Preco invalido.\n";  
        }
        
         if (!isStringNumeric(Custo) ){
            Mensagem = Mensagem + "Custo invalido.\n";  
        }
        
        
        if(!Produto.equals("")){
            if(ProdutoExiste(Produto)){
                Mensagem = Mensagem + "Produto já cadastrado.\n";
                TxtNome.setText("");
                LimparFormulario();
            }
        }
        
        if (!Mensagem.equals("")){
            JOptionPane.showMessageDialog(this, Mensagem);
            return false;
        }else {
            return true;
        }
    }
  
  private boolean ProdutoExiste(String ProdutoDigitado){
      this.conectar.conectaBanco();
      String ProdutoEncontrado  = "";
        
        try {
           this.conectar.executarSQL("select * from Produto where Produto = " + "'" + ProdutoDigitado + "'" + ";"); //SQL para buscar no Banco
           
           while(this.conectar.getResultSet().next()){
               ProdutoEncontrado = this.conectar.getResultSet().getString(2);// 1 é o ID, 2 o nome
           }
           
           if(ProdutoEncontrado.equals(ProdutoDigitado)){ 
               this.conectar.fechaBanco();
               return true;
           } else{
               this.conectar.fechaBanco();
               return false;
           }
           
        }catch(Exception e){
            System.out.println("Erro ao verificar Produto " +  e.getMessage());
            JOptionPane.showMessageDialog(null, "Erro ao verificar Produto");
            this.conectar.fechaBanco();
            return true;
        }
   }
  
  private void LimparFormulario(){
        TxtNome.setText("");
        TxtDescricao.setText("");
        TxtPreco.setText("");
        TxtCusto.setText("");
        CBoxEsporte.setSelectedIndex(0); 
    }
  
  public boolean isStringNumeric(String s){
      
            try {
                Double.parseDouble(s);
                return true;
            } catch (NumberFormatException ex) {
                return false;
            }
        } 
  
  public static void main(String args[]) {
    /* Set the Nimbus look and feel */
    //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
    /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
     */
    try {
      for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
        if ("Nimbus".equals(info.getName())) {
          javax.swing.UIManager.setLookAndFeel(info.getClassName());
          break;
        }
      }
    } catch (ClassNotFoundException ex) {
      java.util.logging.Logger.getLogger(TelaCadastroProduto.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
    } catch (InstantiationException ex) {
      java.util.logging.Logger.getLogger(TelaCadastroProduto.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
    } catch (IllegalAccessException ex) {
      java.util.logging.Logger.getLogger(TelaCadastroProduto.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
    } catch (javax.swing.UnsupportedLookAndFeelException ex) {
      java.util.logging.Logger.getLogger(TelaCadastroProduto.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
    }
    //</editor-fold>
    //</editor-fold>

    /* Create and display the form */
    java.awt.EventQueue.invokeLater(new Runnable() {
      public void run() {
        new TelaCadastroProduto().setVisible(true);
      }
    });
  }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton BtnConsulta;
    private javax.swing.JButton Btn_Cadastrar;
    private javax.swing.JButton Btn_Limpar;
    private javax.swing.JButton Btn_MenuPrincipal;
    private javax.swing.JComboBox<String> CBoxEsporte;
    private javax.swing.JLabel LblCusto;
    private javax.swing.JLabel LblDescricao;
    private javax.swing.JLabel LblEsporte;
    private javax.swing.JLabel LblNome;
    private javax.swing.JLabel LblPreco;
    private javax.swing.JLabel LblTitulo;
    private javax.swing.JScrollPane ScrollPanelDescricao;
    private javax.swing.JTextField TxtCusto;
    private javax.swing.JTextArea TxtDescricao;
    private javax.swing.JTextField TxtNome;
    private javax.swing.JTextField TxtPreco;
    private javax.swing.JPanel jPanel1;
    // End of variables declaration//GEN-END:variables
}
