
package planet_sports;

import Conexao.MySQL;
import Objetos.Produto;
import java.awt.event.ActionListener;
import java.util.Enumeration;
import javax.swing.AbstractButton;
import javax.swing.ButtonGroup;
import javax.swing.ButtonModel;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;


public class TelaCatalogo extends javax.swing.JFrame {

    MySQL conectar = new MySQL();
    Produto ProdutoBuscado = new Produto() ;
    boolean FormPreenchido = false;
    boolean TelaEditavel = false;
    JOptionPane PopUp = new JOptionPane();
    String User;
    String Permissao;
    
  public TelaCatalogo(String User, String Permissao) {
    initComponents();
    this.User = User;
    this.Permissao = Permissao;
  }
    
  public TelaCatalogo() {
    initComponents();
  }


  @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        BtnGroupEsportes = new javax.swing.ButtonGroup();
        BtnGroupFiltro = new javax.swing.ButtonGroup();
        PnlBackground = new javax.swing.JPanel();
        PnlBotoes = new javax.swing.JPanel();
        BtnVoltar = new javax.swing.JButton();
        BtnDescricao = new javax.swing.JButton();
        BtnLimpar = new javax.swing.JButton();
        BtnCatalogoCompleto = new javax.swing.JButton();
        PnlEsportes = new javax.swing.JPanel();
        LblEsporte = new javax.swing.JLabel();
        PnlEsportes1 = new javax.swing.JPanel();
        jRadioVolei = new javax.swing.JRadioButton();
        JRadioFutebol = new javax.swing.JRadioButton();
        JRadioBasquete = new javax.swing.JRadioButton();
        PnlEsportes3 = new javax.swing.JPanel();
        JRadioCorrida = new javax.swing.JRadioButton();
        JRadioLutas = new javax.swing.JRadioButton();
        JRadioSkate = new javax.swing.JRadioButton();
        PnlPesquisa = new javax.swing.JPanel();
        LblPesquisa = new javax.swing.JLabel();
        TxtPesquisa = new javax.swing.JTextField();
        JRadioCodigo = new javax.swing.JRadioButton();
        JRadioNome = new javax.swing.JRadioButton();
        BtnPesquisar = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        Catalogo = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        PnlBackground.setBackground(new java.awt.Color(255, 204, 102));

        PnlBotoes.setBackground(new java.awt.Color(255, 204, 102));

        BtnVoltar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/planet_sports/Icons/Icone.Voltar.png"))); // NOI18N
        BtnVoltar.setText("Voltar");
        BtnVoltar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnVoltarActionPerformed(evt);
            }
        });

        BtnDescricao.setIcon(new javax.swing.ImageIcon(getClass().getResource("/planet_sports/Icons/Descricao.png"))); // NOI18N
        BtnDescricao.setText("Descrição");
        BtnDescricao.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnDescricaoActionPerformed(evt);
            }
        });

        BtnLimpar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/planet_sports/Icons/Icone.Limpar.png"))); // NOI18N
        BtnLimpar.setText("Limpar");
        BtnLimpar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnLimparActionPerformed(evt);
            }
        });

        BtnCatalogoCompleto.setText("Mostrar Tudo");
        BtnCatalogoCompleto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnCatalogoCompletoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout PnlBotoesLayout = new javax.swing.GroupLayout(PnlBotoes);
        PnlBotoes.setLayout(PnlBotoesLayout);
        PnlBotoesLayout.setHorizontalGroup(
            PnlBotoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PnlBotoesLayout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addComponent(BtnDescricao)
                .addGap(30, 30, 30)
                .addComponent(BtnCatalogoCompleto)
                .addGap(30, 30, 30)
                .addComponent(BtnLimpar)
                .addGap(30, 30, 30)
                .addComponent(BtnVoltar)
                .addGap(18, 18, 18))
        );
        PnlBotoesLayout.setVerticalGroup(
            PnlBotoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PnlBotoesLayout.createSequentialGroup()
                .addGap(17, 17, 17)
                .addGroup(PnlBotoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(PnlBotoesLayout.createSequentialGroup()
                        .addGroup(PnlBotoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(BtnCatalogoCompleto, javax.swing.GroupLayout.DEFAULT_SIZE, 27, Short.MAX_VALUE)
                            .addComponent(BtnDescricao, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addContainerGap())
                    .addGroup(PnlBotoesLayout.createSequentialGroup()
                        .addGroup(PnlBotoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(BtnVoltar)
                            .addComponent(BtnLimpar))
                        .addGap(12, 12, 12))))
        );

        PnlEsportes.setBackground(new java.awt.Color(255, 204, 102));

        LblEsporte.setText("Esporte:");

        PnlEsportes1.setBackground(new java.awt.Color(255, 204, 102));

        jRadioVolei.setBackground(new java.awt.Color(255, 204, 102));
        BtnGroupEsportes.add(jRadioVolei);
        jRadioVolei.setText("Volei");

        JRadioFutebol.setBackground(new java.awt.Color(255, 204, 102));
        BtnGroupEsportes.add(JRadioFutebol);
        JRadioFutebol.setText("Futebol");

        JRadioBasquete.setBackground(new java.awt.Color(255, 204, 102));
        BtnGroupEsportes.add(JRadioBasquete);
        JRadioBasquete.setText("Basquete");

        javax.swing.GroupLayout PnlEsportes1Layout = new javax.swing.GroupLayout(PnlEsportes1);
        PnlEsportes1.setLayout(PnlEsportes1Layout);
        PnlEsportes1Layout.setHorizontalGroup(
            PnlEsportes1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PnlEsportes1Layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addGroup(PnlEsportes1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(JRadioBasquete)
                    .addComponent(JRadioFutebol)
                    .addComponent(jRadioVolei)))
        );
        PnlEsportes1Layout.setVerticalGroup(
            PnlEsportes1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PnlEsportes1Layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(jRadioVolei)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(JRadioFutebol)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(JRadioBasquete)
                .addGap(0, 0, 0))
        );

        PnlEsportes3.setBackground(new java.awt.Color(255, 204, 102));

        JRadioCorrida.setBackground(new java.awt.Color(255, 204, 102));
        BtnGroupEsportes.add(JRadioCorrida);
        JRadioCorrida.setText("Corrida/Caminhada");

        JRadioLutas.setBackground(new java.awt.Color(255, 204, 102));
        BtnGroupEsportes.add(JRadioLutas);
        JRadioLutas.setText("Luta/Artes Marciais");

        JRadioSkate.setBackground(new java.awt.Color(255, 204, 102));
        BtnGroupEsportes.add(JRadioSkate);
        JRadioSkate.setText("Skate");

        javax.swing.GroupLayout PnlEsportes3Layout = new javax.swing.GroupLayout(PnlEsportes3);
        PnlEsportes3.setLayout(PnlEsportes3Layout);
        PnlEsportes3Layout.setHorizontalGroup(
            PnlEsportes3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PnlEsportes3Layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addGroup(PnlEsportes3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(JRadioSkate)
                    .addComponent(JRadioLutas)
                    .addComponent(JRadioCorrida)))
        );
        PnlEsportes3Layout.setVerticalGroup(
            PnlEsportes3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PnlEsportes3Layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(JRadioCorrida, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(JRadioLutas)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(JRadioSkate))
        );

        javax.swing.GroupLayout PnlEsportesLayout = new javax.swing.GroupLayout(PnlEsportes);
        PnlEsportes.setLayout(PnlEsportesLayout);
        PnlEsportesLayout.setHorizontalGroup(
            PnlEsportesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PnlEsportesLayout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addGroup(PnlEsportesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(LblEsporte)
                    .addGroup(PnlEsportesLayout.createSequentialGroup()
                        .addComponent(PnlEsportes1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(PnlEsportes3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18))
        );
        PnlEsportesLayout.setVerticalGroup(
            PnlEsportesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PnlEsportesLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(LblEsporte)
                .addGap(0, 0, Short.MAX_VALUE)
                .addGroup(PnlEsportesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(PnlEsportes3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(PnlEsportes1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10))
        );

        PnlPesquisa.setBackground(new java.awt.Color(255, 204, 102));

        LblPesquisa.setText("Pesquisa:");

        JRadioCodigo.setBackground(new java.awt.Color(255, 204, 102));
        BtnGroupFiltro.add(JRadioCodigo);
        JRadioCodigo.setText("Codigo");
        JRadioCodigo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JRadioCodigoActionPerformed(evt);
            }
        });

        JRadioNome.setBackground(new java.awt.Color(255, 204, 102));
        BtnGroupFiltro.add(JRadioNome);
        JRadioNome.setText("Nome");

        BtnPesquisar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/planet_sports/Icons/Icone.Lupa2.png"))); // NOI18N
        BtnPesquisar.setText("Pesquisar");
        BtnPesquisar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnPesquisarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout PnlPesquisaLayout = new javax.swing.GroupLayout(PnlPesquisa);
        PnlPesquisa.setLayout(PnlPesquisaLayout);
        PnlPesquisaLayout.setHorizontalGroup(
            PnlPesquisaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PnlPesquisaLayout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addGroup(PnlPesquisaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(BtnPesquisar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(PnlPesquisaLayout.createSequentialGroup()
                        .addComponent(LblPesquisa)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 10, Short.MAX_VALUE)
                        .addComponent(JRadioCodigo)
                        .addGap(32, 32, 32)
                        .addComponent(JRadioNome))
                    .addComponent(TxtPesquisa))
                .addGap(18, 18, 18))
        );
        PnlPesquisaLayout.setVerticalGroup(
            PnlPesquisaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PnlPesquisaLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addGroup(PnlPesquisaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(JRadioCodigo, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(PnlPesquisaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(JRadioNome)
                        .addComponent(LblPesquisa)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(TxtPesquisa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(BtnPesquisar)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jScrollPane1.setBackground(new java.awt.Color(255, 204, 102));
        jScrollPane1.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        Catalogo.setBackground(new java.awt.Color(255, 204, 102));
        Catalogo.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Codigo", "Produto", "Descrição", "Preço", "Esporte"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(Catalogo);

        javax.swing.GroupLayout PnlBackgroundLayout = new javax.swing.GroupLayout(PnlBackground);
        PnlBackground.setLayout(PnlBackgroundLayout);
        PnlBackgroundLayout.setHorizontalGroup(
            PnlBackgroundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PnlBackgroundLayout.createSequentialGroup()
                .addComponent(PnlPesquisa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(PnlEsportes, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addComponent(jScrollPane1)
            .addComponent(PnlBotoes, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        PnlBackgroundLayout.setVerticalGroup(
            PnlBackgroundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PnlBackgroundLayout.createSequentialGroup()
                .addComponent(PnlBotoes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(PnlBackgroundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(PnlPesquisa, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(PnlEsportes, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 139, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(PnlBackground, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(PnlBackground, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void BtnLimparActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnLimparActionPerformed
        LimparFormulario();
    }//GEN-LAST:event_BtnLimparActionPerformed

    private void BtnPesquisarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnPesquisarActionPerformed
        
        if(isInfoOK()){
            DefaultTableModel ModeloTabela = (DefaultTableModel) Catalogo.getModel();
            ModeloTabela.setRowCount(0);
            BuscarProduto(SelecionaSQLString()); 
        }
        
    }//GEN-LAST:event_BtnPesquisarActionPerformed
    
    private void JRadioCodigoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JRadioCodigoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_JRadioCodigoActionPerformed

    private void BtnVoltarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnVoltarActionPerformed
        MainScreen Menu = new MainScreen(User,Permissao);
        Menu.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_BtnVoltarActionPerformed

    private void BtnCatalogoCompletoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnCatalogoCompletoActionPerformed
        LimparFormulario();
        DefaultTableModel ModeloTabela = (DefaultTableModel) Catalogo.getModel();
        ModeloTabela.setRowCount(0);
        BuscarProduto(Tudo());
    }//GEN-LAST:event_BtnCatalogoCompletoActionPerformed

    private void BtnDescricaoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnDescricaoActionPerformed
        int Linha = Catalogo.getSelectedRow();
        
        if(Linha >= 0){
            String Descricao = Catalogo.getModel().getValueAt(Linha, 2).toString();
            JOptionPane.showMessageDialog(this, Descricao);
        }else{
            JOptionPane.showMessageDialog(this, "Selecione um item do catálogo");
        }
        
    }//GEN-LAST:event_BtnDescricaoActionPerformed
        
    private String Tudo(){
        String SQL = "SELECT "
                    + "idProduto,"
                    + "Produto,"                    
                    + "Descricao,"
                    + "Preco,"
                    + "Esporte"
                 + " FROM"
                     + " Produto;";
        
        return SQL;
    }
    
    private String TotalmenteFiltrado(){
        
        String Opcao;
    
        
        if(JRadioNome.isSelected()){
            Opcao = "Produto";
        }else{
            Opcao = "idProduto";
        }
        
        
        String SQL = "SELECT "
                    + "idProduto,"
                    + "Produto,"                    
                    + "Descricao,"
                    + "Preco,"
                    + "Esporte"
                 + " FROM"
                     + " Produto"
                 + " WHERE " + Opcao + " LIKE '%" + TxtPesquisa.getText() + "%'"
                 + " AND Esporte ='" + EsporteSelecionado() + "';";
        
        return SQL;
    }
    
    private String FiltroTexto(){
        
        String Opcao;
    
        
        if(JRadioNome.isSelected()){
            Opcao = "Produto";
        }else{
            Opcao = "idProduto";
        }
        
        
        String SQL = "SELECT "
                    + "idProduto,"
                    + "Produto,"                    
                    + "Descricao,"
                    + "Preco,"
                    + "Esporte"
                 + " FROM"
                     + " Produto"
                 + " WHERE " + Opcao + " LIKE '%" + TxtPesquisa.getText() + "%';";
        
        return SQL;
    }
        
    private String EsporteSelecionado(){
        for (Enumeration<AbstractButton> buttons = BtnGroupEsportes.getElements(); buttons.hasMoreElements();) {
            AbstractButton Selecionado = buttons.nextElement();

            if (Selecionado.isSelected()) {
                return Selecionado.getText();
            }
        }
        
        return null;
    }
    
    private String FiltroEsporte(){
        
        String SQL = "SELECT "
                    + "idProduto,"
                    + "Produto,"                    
                    + "Descricao,"
                    + "Preco,"
                    + "Esporte"
                 + " FROM"
                     + " Produto"
                 + " WHERE Esporte = '" + EsporteSelecionado() + "';";
        
        return SQL;
    }
    
    private String SelecionaSQLString(){

        if(isEsporteFiltrado() && isTextoFiltrado()){
            return TotalmenteFiltrado();
        }
        
        if(!isEsporteFiltrado() && isTextoFiltrado()){
            return FiltroTexto();
        }
        
        if(isEsporteFiltrado() && !isTextoFiltrado()){
            return FiltroEsporte();
        }
        
        return null;
    }
    
    private boolean isTextoFiltrado(){
        return JRadioNome.isSelected() || JRadioCodigo.isSelected();
    }
    
    private boolean isEsporteFiltrado(){
        
        for (Enumeration<AbstractButton> buttons = BtnGroupEsportes.getElements(); buttons.hasMoreElements();) {
            AbstractButton Selecionado = buttons.nextElement();

            if (Selecionado.isSelected()) {
                return true;
            }
        }
        
        return false;
    }
        
    private void BuscarProduto(String SQL){
        this.conectar.conectaBanco();
        String Codigo = "";
        String Produto;
        String Descricao;
        String Preco;
        String Esporte;
        
        
        try {
            this.conectar.executarSQL(SQL);
            
            while(this.conectar.getResultSet().next()){
                Codigo = String.valueOf(this.conectar.getResultSet().getInt(1));
                Produto = this.conectar.getResultSet().getString(2);
                Descricao = this.conectar.getResultSet().getString(3);
                Preco = String.valueOf(this.conectar.getResultSet().getFloat(4));
                Esporte = this.conectar.getResultSet().getString(5);
                
                String DadosCatalogo[] = {Codigo, Produto, Descricao, Preco, Esporte};
                DefaultTableModel ModeloTabela = (DefaultTableModel)Catalogo.getModel();
                
                ModeloTabela.addRow(DadosCatalogo);
           }
                
           if(Codigo.equals("")){
                LimparFormulario();
                JOptionPane.showMessageDialog(null, "Produto não encontrado!");
           }else{
               FormPreenchido = true;
           }

        } catch (Exception e) {            
            System.out.println("Erro ao consultar produto " +  e.getMessage());
            JOptionPane.showMessageDialog(null, "Erro ao buscar produto");
        }finally{
            this.conectar.fechaBanco();   
        }               
    }
        
    private void LimparFormulario(){  
        TxtPesquisa.setText("");
        BtnGroupEsportes.clearSelection();
        BtnGroupFiltro.clearSelection();
        
        DefaultTableModel ModeloTabela = (DefaultTableModel) Catalogo.getModel();
        ModeloTabela.setRowCount(0);
        
    }
   
    private boolean isInfoOK(){
        
        String Mensagem = "";
        
        if(!TxtPesquisa.getText().equals("") && !(JRadioCodigo.isSelected() || JRadioNome.isSelected())){
            Mensagem = Mensagem + "Selecione o tipo de pesquisa\n";
        }
        
        if((JRadioCodigo.isSelected() || JRadioNome.isSelected()) && TxtPesquisa.getText().equals("")){
            Mensagem = Mensagem + "Digite o termo de pesquisa\n";
        }
        
        if(!isEsporteFiltrado()){
           if(!(JRadioCodigo.isSelected() || JRadioNome.isSelected()) && TxtPesquisa.getText().equals("")){
                Mensagem = Mensagem + "Preencha os dados de pesquisa\n";
            } 
        }
        
        if (!Mensagem.equals("")){
            JOptionPane.showMessageDialog(this, Mensagem);
            return false;
        }else {
            return true;
        }
    }
    
    
    
  public static void main(String args[]) {
    /* Set the Nimbus look and feel */
    //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
    /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
     */
    try {
      for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
        if ("Nimbus".equals(info.getName())) {
          javax.swing.UIManager.setLookAndFeel(info.getClassName());
          break;
        }
      }
    } catch (ClassNotFoundException ex) {
      java.util.logging.Logger.getLogger(TelaCatalogo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
    } catch (InstantiationException ex) {
      java.util.logging.Logger.getLogger(TelaCatalogo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
    } catch (IllegalAccessException ex) {
      java.util.logging.Logger.getLogger(TelaCatalogo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
    } catch (javax.swing.UnsupportedLookAndFeelException ex) {
      java.util.logging.Logger.getLogger(TelaCatalogo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
    }
    //</editor-fold>

    /* Create and display the form */
    java.awt.EventQueue.invokeLater(new Runnable() {
      public void run() {
        new TelaCatalogo().setVisible(true);
      }
    });
  }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton BtnCatalogoCompleto;
    private javax.swing.JButton BtnDescricao;
    private javax.swing.ButtonGroup BtnGroupEsportes;
    private javax.swing.ButtonGroup BtnGroupFiltro;
    private javax.swing.JButton BtnLimpar;
    private javax.swing.JButton BtnPesquisar;
    private javax.swing.JButton BtnVoltar;
    private javax.swing.JTable Catalogo;
    private javax.swing.JRadioButton JRadioBasquete;
    private javax.swing.JRadioButton JRadioCodigo;
    private javax.swing.JRadioButton JRadioCorrida;
    private javax.swing.JRadioButton JRadioFutebol;
    private javax.swing.JRadioButton JRadioLutas;
    private javax.swing.JRadioButton JRadioNome;
    private javax.swing.JRadioButton JRadioSkate;
    private javax.swing.JLabel LblEsporte;
    private javax.swing.JLabel LblPesquisa;
    private javax.swing.JPanel PnlBackground;
    private javax.swing.JPanel PnlBotoes;
    private javax.swing.JPanel PnlEsportes;
    private javax.swing.JPanel PnlEsportes1;
    private javax.swing.JPanel PnlEsportes3;
    private javax.swing.JPanel PnlPesquisa;
    private javax.swing.JTextField TxtPesquisa;
    private javax.swing.JRadioButton jRadioVolei;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables
}
