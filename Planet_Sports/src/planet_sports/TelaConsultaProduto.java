
package planet_sports;

import Conexao.MySQL;
import Objetos.Produto;
import java.awt.Color;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

public class TelaConsultaProduto extends javax.swing.JFrame {

    MySQL conectar = new MySQL();
    Produto ProdutoBuscado = new Produto() ;
    boolean FormPreenchido = false;
    boolean TelaEditavel = false;
    JOptionPane PopUp = new JOptionPane();
    String User;
    String Permissao;
    
  public TelaConsultaProduto(String User, String Permissao) {
    initComponents();
    this.User = User;
    this.Permissao = Permissao;
    BtnDeletar.setVisible(false);
  }
    
    
    
    public TelaConsultaProduto() {
        initComponents();

    }


    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        BtnGroupPesquisa = new javax.swing.ButtonGroup();
        PnlBackGround = new javax.swing.JPanel();
        LblTitulo = new javax.swing.JLabel();
        PnlPesquisa = new javax.swing.JPanel();
        LblPesquisar = new javax.swing.JLabel();
        JBtnNome = new javax.swing.JRadioButton();
        JBtnCodigo = new javax.swing.JRadioButton();
        TxtPesquisar = new javax.swing.JTextField();
        BtnPesquisar = new javax.swing.JButton();
        PnlInfo = new javax.swing.JPanel();
        LblProduto = new javax.swing.JLabel();
        TxtProduto = new javax.swing.JTextField();
        LblDescricao = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        TxtDescricao = new javax.swing.JTextArea();
        LblPreco = new javax.swing.JLabel();
        TxtPreco = new javax.swing.JTextField();
        TxtCusto = new javax.swing.JTextField();
        LblCusto = new javax.swing.JLabel();
        LblEsporte = new javax.swing.JLabel();
        CBoxEsporte = new javax.swing.JComboBox<>();
        PnlBotoes = new javax.swing.JPanel();
        BtnLimpar = new javax.swing.JButton();
        BtnEditar = new javax.swing.JButton();
        BtnVoltar = new javax.swing.JButton();
        PnlBotaoDeletar = new javax.swing.JPanel();
        BtnDeletar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);
        setSize(new java.awt.Dimension(385, 433));

        PnlBackGround.setBackground(new java.awt.Color(255, 204, 102));
        PnlBackGround.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        PnlBackGround.setPreferredSize(new java.awt.Dimension(560, 380));

        LblTitulo.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        LblTitulo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblTitulo.setText("CONSULTA DE PRODUTO");

        PnlPesquisa.setBackground(new java.awt.Color(255, 204, 102));

        LblPesquisar.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        LblPesquisar.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        LblPesquisar.setText("Pesquisar:");

        JBtnNome.setBackground(new java.awt.Color(255, 204, 102));
        BtnGroupPesquisa.add(JBtnNome);
        JBtnNome.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        JBtnNome.setText("Nome");

        JBtnCodigo.setBackground(new java.awt.Color(255, 204, 102));
        BtnGroupPesquisa.add(JBtnCodigo);
        JBtnCodigo.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        JBtnCodigo.setText("Codigo");

        BtnPesquisar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/planet_sports/Icons/Icone.Lupa2.png"))); // NOI18N
        BtnPesquisar.setText("Pesquisar");
        BtnPesquisar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnPesquisarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout PnlPesquisaLayout = new javax.swing.GroupLayout(PnlPesquisa);
        PnlPesquisa.setLayout(PnlPesquisaLayout);
        PnlPesquisaLayout.setHorizontalGroup(
            PnlPesquisaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PnlPesquisaLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(LblPesquisar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(PnlPesquisaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(PnlPesquisaLayout.createSequentialGroup()
                        .addComponent(JBtnNome)
                        .addGap(12, 12, 12)
                        .addComponent(JBtnCodigo)
                        .addGap(18, 18, 18)
                        .addComponent(BtnPesquisar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(TxtPesquisar))
                .addContainerGap())
        );
        PnlPesquisaLayout.setVerticalGroup(
            PnlPesquisaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PnlPesquisaLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(PnlPesquisaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(LblPesquisar)
                    .addComponent(JBtnNome)
                    .addComponent(JBtnCodigo)
                    .addComponent(BtnPesquisar))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(TxtPesquisar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        PnlInfo.setBackground(new java.awt.Color(255, 204, 102));

        LblProduto.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        LblProduto.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        LblProduto.setText("Produto");
        LblProduto.setPreferredSize(new java.awt.Dimension(71, 17));

        TxtProduto.setBackground(new java.awt.Color(204, 204, 204));

        LblDescricao.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        LblDescricao.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        LblDescricao.setText("Descricao");
        LblDescricao.setPreferredSize(new java.awt.Dimension(71, 17));

        jScrollPane1.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        jScrollPane1.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

        TxtDescricao.setBackground(new java.awt.Color(204, 204, 204));
        TxtDescricao.setColumns(20);
        TxtDescricao.setLineWrap(true);
        TxtDescricao.setRows(5);
        jScrollPane1.setViewportView(TxtDescricao);

        LblPreco.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        LblPreco.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        LblPreco.setText("Preco");
        LblPreco.setMaximumSize(new java.awt.Dimension(71, 17));
        LblPreco.setPreferredSize(new java.awt.Dimension(71, 17));

        TxtPreco.setBackground(new java.awt.Color(204, 204, 204));
        TxtPreco.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TxtPrecoActionPerformed(evt);
            }
        });

        TxtCusto.setBackground(new java.awt.Color(204, 204, 204));
        TxtCusto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TxtCustoActionPerformed(evt);
            }
        });

        LblCusto.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        LblCusto.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        LblCusto.setText("Custo");

        LblEsporte.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        LblEsporte.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        LblEsporte.setText("Esporte");

        CBoxEsporte.setBackground(new java.awt.Color(204, 204, 204));
        CBoxEsporte.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "", "Volei", "Futebol", "Basquete", "Corrida/Caminhada", "Luta", "Skate" }));

        PnlBotoes.setBackground(new java.awt.Color(255, 204, 102));

        BtnLimpar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/planet_sports/Icons/Icone.Limpar.png"))); // NOI18N
        BtnLimpar.setText("Limpar");
        BtnLimpar.setToolTipText("");
        BtnLimpar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnLimparActionPerformed(evt);
            }
        });

        BtnEditar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/planet_sports/Icons/Icone.Editar.png"))); // NOI18N
        BtnEditar.setText("Editar");
        BtnEditar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnEditarActionPerformed(evt);
            }
        });

        BtnVoltar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/planet_sports/Icons/Icone.Voltar.png"))); // NOI18N
        BtnVoltar.setText("Voltar");
        BtnVoltar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnVoltarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout PnlBotoesLayout = new javax.swing.GroupLayout(PnlBotoes);
        PnlBotoes.setLayout(PnlBotoesLayout);
        PnlBotoesLayout.setHorizontalGroup(
            PnlBotoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PnlBotoesLayout.createSequentialGroup()
                .addGap(0, 10, Short.MAX_VALUE)
                .addComponent(BtnEditar, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(BtnLimpar, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(BtnVoltar, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(8, 8, 8))
        );
        PnlBotoesLayout.setVerticalGroup(
            PnlBotoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, PnlBotoesLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(PnlBotoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(BtnEditar)
                    .addComponent(BtnLimpar)
                    .addComponent(BtnVoltar))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        PnlBotaoDeletar.setBackground(new java.awt.Color(255, 204, 102));

        BtnDeletar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/planet_sports/Icons/Icone.Deletar.png"))); // NOI18N
        BtnDeletar.setText("Deletar");
        BtnDeletar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnDeletarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout PnlBotaoDeletarLayout = new javax.swing.GroupLayout(PnlBotaoDeletar);
        PnlBotaoDeletar.setLayout(PnlBotaoDeletarLayout);
        PnlBotaoDeletarLayout.setHorizontalGroup(
            PnlBotaoDeletarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PnlBotaoDeletarLayout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addComponent(BtnDeletar)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        PnlBotaoDeletarLayout.setVerticalGroup(
            PnlBotaoDeletarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PnlBotaoDeletarLayout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addComponent(BtnDeletar, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout PnlInfoLayout = new javax.swing.GroupLayout(PnlInfo);
        PnlInfo.setLayout(PnlInfoLayout);
        PnlInfoLayout.setHorizontalGroup(
            PnlInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PnlInfoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(PnlInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(PnlBotoes, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(PnlInfoLayout.createSequentialGroup()
                        .addGroup(PnlInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, PnlInfoLayout.createSequentialGroup()
                                .addGroup(PnlInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(LblDescricao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(LblProduto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(PnlInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(TxtProduto)
                                    .addComponent(jScrollPane1)))
                            .addGroup(PnlInfoLayout.createSequentialGroup()
                                .addGroup(PnlInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(PnlInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(LblEsporte, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(LblCusto, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(LblPreco, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(PnlInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(PnlInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                        .addComponent(TxtCusto, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(CBoxEsporte, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(TxtPreco, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addComponent(PnlBotaoDeletar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                        .addContainerGap())))
        );
        PnlInfoLayout.setVerticalGroup(
            PnlInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PnlInfoLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(PnlInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(LblProduto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(TxtProduto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(PnlInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(LblDescricao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(PnlInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(PnlInfoLayout.createSequentialGroup()
                        .addGroup(PnlInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(TxtPreco, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(LblPreco, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(PnlInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(TxtCusto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(LblCusto))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(PnlInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(CBoxEsporte, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(LblEsporte)))
                    .addComponent(PnlBotaoDeletar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(PnlBotoes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        javax.swing.GroupLayout PnlBackGroundLayout = new javax.swing.GroupLayout(PnlBackGround);
        PnlBackGround.setLayout(PnlBackGroundLayout);
        PnlBackGroundLayout.setHorizontalGroup(
            PnlBackGroundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PnlBackGroundLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(PnlBackGroundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(PnlInfo, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(PnlPesquisa, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(LblTitulo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        PnlBackGroundLayout.setVerticalGroup(
            PnlBackGroundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PnlBackGroundLayout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addComponent(LblTitulo)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(PnlPesquisa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(PnlInfo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(45, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(PnlBackGround, javax.swing.GroupLayout.DEFAULT_SIZE, 385, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(PnlBackGround, javax.swing.GroupLayout.DEFAULT_SIZE, 467, Short.MAX_VALUE)
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void TxtPrecoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TxtPrecoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_TxtPrecoActionPerformed

    private void TxtCustoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TxtCustoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_TxtCustoActionPerformed

    private void BtnEditarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnEditarActionPerformed
        
        if(TelaEditavel){
            if(isInfoOK()){
               AtualizaObjetoProduto(ProdutoBuscado);
               EditarProduto(ProdutoBuscado);
            }
        }else{
            if(FormPreenchido){
                setTelaEditavel();
            }else{
               JOptionPane.showMessageDialog(null, "Nenhum produto selecionado."); 
            }
        }
    }//GEN-LAST:event_BtnEditarActionPerformed

    private void BtnVoltarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnVoltarActionPerformed
        MainScreen Menu = new MainScreen(User, Permissao);
        Menu.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_BtnVoltarActionPerformed

    private void BtnLimparActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnLimparActionPerformed
        
        if(TelaEditavel){
            setTelaConsulta();
            LimparFormulario();
        }else{
            LimparFormulario();
        }
        
    }//GEN-LAST:event_BtnLimparActionPerformed

    private void BtnPesquisarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnPesquisarActionPerformed
        
        if(TelaEditavel){
            setTelaConsulta();
            LimparFormulario();
        }
        
        if(((JBtnCodigo.isSelected() || JBtnNome.isSelected()) && !TxtPesquisar.getText().equals(""))){
            BuscarProduto(ProdutoBuscado); 
        }else{
            JOptionPane.showMessageDialog(this, "Preencha os dados para pesquisa");
        }
        
        
    }//GEN-LAST:event_BtnPesquisarActionPerformed

    private void BtnDeletarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnDeletarActionPerformed
        Object Opcoes[] = {"Sim", "Cancelar"};
                
        if(PopupOpcao(Opcoes)){
            DeletarProduto(ProdutoBuscado);
        }
    }//GEN-LAST:event_BtnDeletarActionPerformed
    
    private void DeletarProduto(Produto ProdutoBuscado){
        this.conectar.conectaBanco();
        
        String Opcao;
        
        if(JBtnNome.isSelected()){
            Opcao = "Produto";
        }else{
            Opcao = "idProduto";
        }
        
        try {            
            this.conectar.updateSQL(
                "DELETE FROM Produto "
                + " WHERE "
                    + Opcao + " = '" + ProdutoBuscado.getProduto() + "'"
                + ";"            
            );
            
        } catch (Exception e) {
            System.out.println("Erro ao deletar produto " +  e.getMessage());
            JOptionPane.showMessageDialog(null, "Erro ao deletar produto");
        }finally{
            this.conectar.fechaBanco();
            LimparFormulario();
            setTelaConsulta();
            JOptionPane.showMessageDialog(null, "Produto deletado com sucesso");            
        }     
        
    }
    
    public void EditarProduto(Produto ProdutoBuscado){
        
        this.conectar.conectaBanco();
        
        String Opcao;
        
        if(JBtnNome.isSelected()){
            Opcao = "Produto";
        }else{
            Opcao = "idProduto";
        }
        
        try {
            this.conectar.updateSQL(
                "UPDATE Produto SET "                    
                    + "Produto = '" + TxtProduto.getText() + "',"
                    + "Descricao = '" + ProdutoBuscado.getDescricao() + "',"
                    + "Preco = '" + ProdutoBuscado.getPreco() + "',"
                    + "Custo = '" + ProdutoBuscado.getCusto() + "',"                   
                    + "Esporte = '" + ProdutoBuscado.getEsporte() + "'"
                + " WHERE "
                    + Opcao + " = '" + ProdutoBuscado.getProduto() + "'"
                + ";"
            );
        }catch(Exception e){
            System.out.println("Erro ao atualizar produto " +  e.getMessage());
            JOptionPane.showMessageDialog(null, "Erro ao atualizar produto");
        }finally{
            this.conectar.fechaBanco();
            LimparFormulario();
            setTelaConsulta();
            JOptionPane.showMessageDialog(null, "Produto atualizado com sucesso");
        }
        
    }
    
    private void AtualizaObjetoProduto(Produto ProdutoBuscado){
        ProdutoBuscado.setDescricao(TxtDescricao.getText());
        ProdutoBuscado.setPreco(Float.valueOf(TxtPreco.getText()));
        ProdutoBuscado.setCusto(Float.valueOf(TxtCusto.getText()));
        ProdutoBuscado.setEsporte(String.valueOf(CBoxEsporte.getSelectedItem()));
    }
    
    private boolean isStringNumeric(String s){
      
        try {
            Double.parseDouble(s);
            return true;
        } catch (NumberFormatException ex) {
            return false;
        }
    }     
    
    private boolean isInfoOK(){
        
        String Mensagem = "";
        String Produto = TxtProduto.getText();
        String Descricao = TxtDescricao.getText();
        String Esporte = String.valueOf(CBoxEsporte.getSelectedItem());
        String Preco = TxtPreco.getText();
        String Custo = TxtCusto.getText();
        
        
        if( Produto.equals("")     ||
            Descricao.equals("")        ||
            Esporte.equals("")   ||
            Preco.equals("")         ||
            Custo.equals("")){
            
            Mensagem = Mensagem + "Preencha todos os campos.\n";            
        }
        
        if (!isStringNumeric(Preco)){
            Mensagem = Mensagem + "Preco invalido.\n";  
        }
        
         if (!isStringNumeric(Custo) ){
            Mensagem = Mensagem + "Custo invalido.\n";  
        }
        
        
        if (!Mensagem.equals("")){
            JOptionPane.showMessageDialog(this, Mensagem);
            return false;
        }else {
            return true;
        }
    }
    
    private void LimparFormulario(){
        TxtPesquisar.setText("");
        TxtProduto.setText("");
        TxtDescricao.setText("");
        TxtPreco.setText("");
        TxtCusto.setText("");
        CBoxEsporte.setSelectedIndex(0);
        FormPreenchido = false;
        JBtnNome.setSelected(false);
        JBtnCodigo.setSelected(false);
        setTelaConsulta();
        ProdutoBuscado.Limpar();
    }
    
    private void setTelaEditavel(){
        
        TelaEditavel = true;
        TxtPesquisar.setBackground(new Color(204,204,204));
        TxtProduto.setBackground(new Color(255,255,255));
        TxtDescricao.setBackground(new Color(255,255,255));
        TxtPreco.setBackground(new Color(255,255,255));
        TxtCusto.setBackground(new Color(255,255,255));

        BtnEditar.setText("Atualizar");
        BtnEditar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/planet_sports/Icons/Icone.Atualizar.png")));
        BtnLimpar.setText("Cancelar");
        BtnDeletar.setVisible(false);
    }
    
    private void setTelaConsulta(){
        TelaEditavel = false;
        TxtPesquisar.setBackground(new Color(255,255,255));
        TxtProduto.setBackground(new Color(204,204,204));
        TxtDescricao.setBackground(new Color(204,204,204));
        TxtPreco.setBackground(new Color(204,204,204));
        TxtCusto.setBackground(new Color(204,204,204));

        BtnEditar.setText("Editar");
        BtnEditar.setIcon(new ImageIcon("/planet_sports/Icons/Icone.Editar.png"));
        BtnLimpar.setText("Limpar");
        BtnDeletar.setVisible(false);
    }
    
    private void BuscarProduto(Produto ProdutoBuscado){
        this.conectar.conectaBanco();
        String Opcao;
        
        if(JBtnNome.isSelected()){
            Opcao = "Produto";
        }else{
            Opcao = "idProduto";
        }
        
        try {
            this.conectar.executarSQL(
                   "SELECT "
                    + "Produto,"                    
                    + "Descricao,"
                    + "Preco,"
                    + "Custo,"
                    + "Esporte"
                 + " FROM"
                     + " Produto"
                 + " WHERE " + Opcao + " LIKE '%" + TxtPesquisar.getText() + "%';"
            );
            
            while(this.conectar.getResultSet().next()){
                ProdutoBuscado.setProduto(this.conectar.getResultSet().getString(1));
                ProdutoBuscado.setDescricao(this.conectar.getResultSet().getString(2));
                ProdutoBuscado.setPreco(this.conectar.getResultSet().getFloat(3));
                ProdutoBuscado.setCusto(this.conectar.getResultSet().getFloat(4));
                ProdutoBuscado.setEsporte(this.conectar.getResultSet().getString(5));
           }
                
           if(ProdutoBuscado.getProduto() == null){
                LimparFormulario();
                JOptionPane.showMessageDialog(null, "Produto não encontrado!");
           }else{
               FormPreenchido = true;
               BtnDeletar.setVisible(true);
           }
           
        } catch (Exception e) {            
            System.out.println("Erro ao consultar produto " +  e.getMessage());
            JOptionPane.showMessageDialog(null, "Erro ao buscar produto");
            
        }finally{
            TxtPesquisar.setText("");
            TxtProduto.setText(ProdutoBuscado.getProduto());
            TxtDescricao.setText(ProdutoBuscado.getDescricao());
            TxtPreco.setText(String.valueOf(ProdutoBuscado.getPreco()));
            TxtCusto.setText(String.valueOf(ProdutoBuscado.getCusto()));
            CBoxEsporte.setSelectedItem(ProdutoBuscado.getEsporte());
            this.conectar.fechaBanco();   
        }               
    }
    
    private boolean PopupOpcao(Object Opcoes[]){
        int Escolha;
        
        Escolha = PopUp.showOptionDialog(this, "Deseja deletar o produto?", "Prestes a deletar um produto!",
        PopUp.YES_NO_OPTION, PopUp.QUESTION_MESSAGE,null,Opcoes,
        Opcoes[0]);
        
        return Escolha == 0;
    }
    
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(TelaConsultaProduto.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(TelaConsultaProduto.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(TelaConsultaProduto.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(TelaConsultaProduto.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new TelaConsultaProduto().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton BtnDeletar;
    private javax.swing.JButton BtnEditar;
    private javax.swing.ButtonGroup BtnGroupPesquisa;
    private javax.swing.JButton BtnLimpar;
    private javax.swing.JButton BtnPesquisar;
    private javax.swing.JButton BtnVoltar;
    private javax.swing.JComboBox<String> CBoxEsporte;
    private javax.swing.JRadioButton JBtnCodigo;
    private javax.swing.JRadioButton JBtnNome;
    private javax.swing.JLabel LblCusto;
    private javax.swing.JLabel LblDescricao;
    private javax.swing.JLabel LblEsporte;
    private javax.swing.JLabel LblPesquisar;
    private javax.swing.JLabel LblPreco;
    private javax.swing.JLabel LblProduto;
    private javax.swing.JLabel LblTitulo;
    private javax.swing.JPanel PnlBackGround;
    private javax.swing.JPanel PnlBotaoDeletar;
    private javax.swing.JPanel PnlBotoes;
    private javax.swing.JPanel PnlInfo;
    private javax.swing.JPanel PnlPesquisa;
    private javax.swing.JTextField TxtCusto;
    private javax.swing.JTextArea TxtDescricao;
    private javax.swing.JTextField TxtPesquisar;
    private javax.swing.JTextField TxtPreco;
    private javax.swing.JTextField TxtProduto;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables
}
