
package planet_sports;

import java.util.Enumeration;
import javax.swing.AbstractButton;
import javax.swing.ButtonModel;
import javax.swing.JOptionPane;

public class TelaPagamento extends javax.swing.JFrame {
    
    TelaVenda TelaVenda;
    float Desconto;
    float ValorTotal;
    float Recebido;

    public TelaPagamento() {
        initComponents();
        setLabelDesconto(false);
        setLabelTroco(false);
    }
    
    public TelaPagamento(TelaVenda Tela_Venda) {
        initComponents();
        setLabelTroco(false);
        setLabelDesconto(false);
        TelaVenda = Tela_Venda;
        LblTotalVenda.setText(TelaVenda.getTotalVenda());
        ValorTotal = Float.valueOf(LblTotalVenda.getText().substring(2));
        LblPrecoFinalN.setText(TelaVenda.getTotalVenda());
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        BtnGroupPagamento = new javax.swing.ButtonGroup();
        PnlBackGround = new javax.swing.JPanel();
        LblPagamento = new javax.swing.JLabel();
        PnlCondicoes = new javax.swing.JPanel();
        jRadioDinheiro = new javax.swing.JRadioButton();
        JRadioCredito = new javax.swing.JRadioButton();
        JRadioDebito = new javax.swing.JRadioButton();
        JRadioBoleto = new javax.swing.JRadioButton();
        JRadioPix = new javax.swing.JRadioButton();
        PnlDesconto = new javax.swing.JPanel();
        LblDesconto = new javax.swing.JLabel();
        TxtDesconto = new javax.swing.JTextField();
        BtnAplicarDesconto = new javax.swing.JButton();
        PnlFinalizar = new javax.swing.JPanel();
        PnlInfoVenda = new javax.swing.JPanel();
        LblTroco = new javax.swing.JLabel();
        LblTrocoN = new javax.swing.JLabel();
        TxtRecebido = new javax.swing.JTextField();
        LblRecebido = new javax.swing.JLabel();
        LblTotal = new javax.swing.JLabel();
        LblTotalVenda = new javax.swing.JLabel();
        LblDescontoTotal = new javax.swing.JLabel();
        LblDescontoN = new javax.swing.JLabel();
        LblPrecoFinal = new javax.swing.JLabel();
        LblPrecoFinalN = new javax.swing.JLabel();
        PnlBotoes = new javax.swing.JPanel();
        BtnFinalizar = new javax.swing.JButton();
        BtnVoltar = new javax.swing.JButton();
        BtnLimpar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        PnlBackGround.setBackground(new java.awt.Color(255, 204, 102));

        LblPagamento.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        LblPagamento.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LblPagamento.setText("PAGAMENTO");

        PnlCondicoes.setBackground(new java.awt.Color(255, 204, 102));

        jRadioDinheiro.setBackground(new java.awt.Color(255, 204, 102));
        BtnGroupPagamento.add(jRadioDinheiro);
        jRadioDinheiro.setText("Dinheiro");
        jRadioDinheiro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioDinheiroActionPerformed(evt);
            }
        });

        JRadioCredito.setBackground(new java.awt.Color(255, 204, 102));
        BtnGroupPagamento.add(JRadioCredito);
        JRadioCredito.setText("Cartão de crédito");
        JRadioCredito.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JRadioCreditoActionPerformed(evt);
            }
        });

        JRadioDebito.setBackground(new java.awt.Color(255, 204, 102));
        BtnGroupPagamento.add(JRadioDebito);
        JRadioDebito.setText("Cartão de débito");
        JRadioDebito.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JRadioDebitoActionPerformed(evt);
            }
        });

        JRadioBoleto.setBackground(new java.awt.Color(255, 204, 102));
        BtnGroupPagamento.add(JRadioBoleto);
        JRadioBoleto.setText("Boleto");
        JRadioBoleto.setToolTipText("");
        JRadioBoleto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JRadioBoletoActionPerformed(evt);
            }
        });

        JRadioPix.setBackground(new java.awt.Color(255, 204, 102));
        BtnGroupPagamento.add(JRadioPix);
        JRadioPix.setText("PIX");
        JRadioPix.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JRadioPixActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout PnlCondicoesLayout = new javax.swing.GroupLayout(PnlCondicoes);
        PnlCondicoes.setLayout(PnlCondicoesLayout);
        PnlCondicoesLayout.setHorizontalGroup(
            PnlCondicoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PnlCondicoesLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(PnlCondicoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(JRadioCredito, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(JRadioDebito, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(JRadioBoleto, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jRadioDinheiro, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(5, 5, 5)
                .addComponent(JRadioPix)
                .addGap(10, 10, 10))
        );
        PnlCondicoesLayout.setVerticalGroup(
            PnlCondicoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, PnlCondicoesLayout.createSequentialGroup()
                .addGap(11, 11, 11)
                .addGroup(PnlCondicoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jRadioDinheiro)
                    .addComponent(JRadioPix))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(JRadioCredito)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(JRadioDebito)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(JRadioBoleto)
                .addGap(11, 11, 11))
        );

        PnlDesconto.setBackground(new java.awt.Color(255, 204, 102));

        LblDesconto.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        LblDesconto.setText("Desconto (%):");

        TxtDesconto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TxtDescontoActionPerformed(evt);
            }
        });

        BtnAplicarDesconto.setText("Aplicar Desconto");
        BtnAplicarDesconto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnAplicarDescontoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout PnlDescontoLayout = new javax.swing.GroupLayout(PnlDesconto);
        PnlDesconto.setLayout(PnlDescontoLayout);
        PnlDescontoLayout.setHorizontalGroup(
            PnlDescontoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, PnlDescontoLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(LblDesconto)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(PnlDescontoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(BtnAplicarDesconto, javax.swing.GroupLayout.DEFAULT_SIZE, 126, Short.MAX_VALUE)
                    .addComponent(TxtDesconto))
                .addContainerGap())
        );
        PnlDescontoLayout.setVerticalGroup(
            PnlDescontoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PnlDescontoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(PnlDescontoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(LblDesconto)
                    .addComponent(TxtDesconto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(BtnAplicarDesconto)
                .addGap(49, 49, 49))
        );

        PnlFinalizar.setBackground(new java.awt.Color(255, 204, 102));
        PnlFinalizar.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        PnlFinalizar.setForeground(new java.awt.Color(255, 204, 102));

        PnlInfoVenda.setBackground(new java.awt.Color(255, 204, 102));

        LblTroco.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        LblTroco.setText("Troco:");
        LblTroco.setToolTipText("");
        LblTroco.setPreferredSize(new java.awt.Dimension(81, 17));

        LblTrocoN.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N

        TxtRecebido.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TxtRecebidoActionPerformed(evt);
            }
        });

        LblRecebido.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        LblRecebido.setText("Recebido:");
        LblRecebido.setToolTipText("");

        LblTotal.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        LblTotal.setText("Total:");
        LblTotal.setToolTipText("");

        LblTotalVenda.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N

        LblDescontoTotal.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        LblDescontoTotal.setText("Desconto:");
        LblDescontoTotal.setToolTipText("");

        LblDescontoN.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N

        LblPrecoFinal.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        LblPrecoFinal.setText("Preço Final:");
        LblPrecoFinal.setToolTipText("");

        LblPrecoFinalN.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N

        javax.swing.GroupLayout PnlInfoVendaLayout = new javax.swing.GroupLayout(PnlInfoVenda);
        PnlInfoVenda.setLayout(PnlInfoVendaLayout);
        PnlInfoVendaLayout.setHorizontalGroup(
            PnlInfoVendaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PnlInfoVendaLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(PnlInfoVendaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(PnlInfoVendaLayout.createSequentialGroup()
                        .addComponent(LblDescontoTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(LblDescontoN, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, PnlInfoVendaLayout.createSequentialGroup()
                        .addComponent(LblPrecoFinal, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(LblPrecoFinalN, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(PnlInfoVendaLayout.createSequentialGroup()
                        .addGroup(PnlInfoVendaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(LblRecebido, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(LblTroco, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(LblTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(PnlInfoVendaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(PnlInfoVendaLayout.createSequentialGroup()
                                .addGroup(PnlInfoVendaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(LblTotalVenda, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(LblTrocoN, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addComponent(TxtRecebido))))
                .addContainerGap())
        );
        PnlInfoVendaLayout.setVerticalGroup(
            PnlInfoVendaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PnlInfoVendaLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(PnlInfoVendaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(LblTotal)
                    .addComponent(LblTotalVenda))
                .addGap(18, 18, Short.MAX_VALUE)
                .addGroup(PnlInfoVendaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(LblDescontoTotal)
                    .addComponent(LblDescontoN))
                .addGap(18, 18, Short.MAX_VALUE)
                .addGroup(PnlInfoVendaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(LblPrecoFinal)
                    .addComponent(LblPrecoFinalN))
                .addGap(18, 18, Short.MAX_VALUE)
                .addGroup(PnlInfoVendaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(LblRecebido)
                    .addComponent(TxtRecebido, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(PnlInfoVendaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(LblTroco, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(LblTrocoN))
                .addContainerGap())
        );

        PnlBotoes.setBackground(new java.awt.Color(255, 204, 102));

        BtnFinalizar.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        BtnFinalizar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/planet_sports/Icons/carrinho-de-compras.png"))); // NOI18N
        BtnFinalizar.setText("Finalizar ");
        BtnFinalizar.setToolTipText("");
        BtnFinalizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnFinalizarActionPerformed(evt);
            }
        });

        BtnVoltar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/planet_sports/Icons/Icone.Voltar.png"))); // NOI18N
        BtnVoltar.setText("Voltar");
        BtnVoltar.setPreferredSize(new java.awt.Dimension(105, 33));
        BtnVoltar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnVoltarActionPerformed(evt);
            }
        });

        BtnLimpar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/planet_sports/Icons/Icone.Limpar.png"))); // NOI18N
        BtnLimpar.setText("Limpar");
        BtnLimpar.setPreferredSize(new java.awt.Dimension(105, 33));
        BtnLimpar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnLimparActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout PnlBotoesLayout = new javax.swing.GroupLayout(PnlBotoes);
        PnlBotoes.setLayout(PnlBotoesLayout);
        PnlBotoesLayout.setHorizontalGroup(
            PnlBotoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PnlBotoesLayout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addGroup(PnlBotoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(BtnVoltar, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(BtnFinalizar, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(BtnLimpar, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 0, 0))
        );
        PnlBotoesLayout.setVerticalGroup(
            PnlBotoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PnlBotoesLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(BtnFinalizar, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(BtnVoltar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(BtnLimpar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout PnlFinalizarLayout = new javax.swing.GroupLayout(PnlFinalizar);
        PnlFinalizar.setLayout(PnlFinalizarLayout);
        PnlFinalizarLayout.setHorizontalGroup(
            PnlFinalizarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PnlFinalizarLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(PnlInfoVenda, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(PnlBotoes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        PnlFinalizarLayout.setVerticalGroup(
            PnlFinalizarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PnlFinalizarLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(PnlFinalizarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(PnlInfoVenda, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(PnlBotoes, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );

        javax.swing.GroupLayout PnlBackGroundLayout = new javax.swing.GroupLayout(PnlBackGround);
        PnlBackGround.setLayout(PnlBackGroundLayout);
        PnlBackGroundLayout.setHorizontalGroup(
            PnlBackGroundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PnlBackGroundLayout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addGroup(PnlBackGroundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(PnlBackGroundLayout.createSequentialGroup()
                        .addComponent(PnlCondicoes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(PnlDesconto, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(LblPagamento, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(PnlFinalizar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18))
        );
        PnlBackGroundLayout.setVerticalGroup(
            PnlBackGroundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PnlBackGroundLayout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addComponent(LblPagamento)
                .addGap(18, 18, 18)
                .addGroup(PnlBackGroundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(PnlCondicoes, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(PnlDesconto, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(PnlFinalizar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(PnlBackGround, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(PnlBackGround, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void JRadioDebitoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JRadioDebitoActionPerformed
        setLabelTroco(false);
    }//GEN-LAST:event_JRadioDebitoActionPerformed

    private void JRadioPixActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JRadioPixActionPerformed
        setLabelTroco(false);
    }//GEN-LAST:event_JRadioPixActionPerformed

    private void JRadioBoletoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JRadioBoletoActionPerformed
        setLabelTroco(false);
    }//GEN-LAST:event_JRadioBoletoActionPerformed

    private void BtnFinalizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnFinalizarActionPerformed
        if(Pagamento(PagamentoSelecionado())){
            FinalizaVenda();
        }
    }//GEN-LAST:event_BtnFinalizarActionPerformed

    private void TxtRecebidoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TxtRecebidoActionPerformed
        String Troco;
        float PrecoFinal = Float.valueOf(LblPrecoFinalN.getText().substring(2));
        
        
        if(isRecebidoOK()){
            Troco = "R$" + String.valueOf(Recebido-PrecoFinal);
            LblTrocoN.setText(Troco);
        }
    }//GEN-LAST:event_TxtRecebidoActionPerformed

    private void BtnVoltarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnVoltarActionPerformed
       TelaVenda.setVisible(true);
       this.setVisible(false);
       this.dispose();
    }//GEN-LAST:event_BtnVoltarActionPerformed

    private void JRadioCreditoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JRadioCreditoActionPerformed
        setLabelTroco(false);
    }//GEN-LAST:event_JRadioCreditoActionPerformed

    private void jRadioDinheiroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioDinheiroActionPerformed
        setLabelTroco(true);
    }//GEN-LAST:event_jRadioDinheiroActionPerformed

    private void BtnAplicarDescontoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnAplicarDescontoActionPerformed
        if(isDescontoOK()){
            setLabelDesconto(true);
            AplicarDesconto();
        }
    }//GEN-LAST:event_BtnAplicarDescontoActionPerformed

    private void BtnLimparActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnLimparActionPerformed
       LimparTela();
    }//GEN-LAST:event_BtnLimparActionPerformed

    private void TxtDescontoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TxtDescontoActionPerformed
      if(isDescontoOK()){
            setLabelDesconto(true);
            AplicarDesconto();
        }  
    }//GEN-LAST:event_TxtDescontoActionPerformed
     
    private String PagamentoSelecionado(){
        for (Enumeration<AbstractButton> buttons = BtnGroupPagamento.getElements(); buttons.hasMoreElements();) {
            AbstractButton Selecionado = buttons.nextElement();

            if (Selecionado.isSelected()) {
                return Selecionado.getText();
            }
        }
        
        return null;
    }
   
    private boolean Pagamento(String PagamentoSelecionado){
        String Mensagem;
        
        if(PagamentoSelecionado == null){
            Mensagem = "Selecione um metodo de pagamento";
            JOptionPane.showMessageDialog(null, Mensagem);
            return false;
        }else if(PagamentoSelecionado.equals("Dinheiro")){
            if(isRecebidoOK()){
               return true;
            }else{
                return false;
            }
        }else{
            return true;
        }
    }
    
    private void FinalizaVenda(){
        String Mensagem = "Obrigado e volte sempre!";
        JOptionPane.showMessageDialog(null, Mensagem);
        TelaVenda Venda = new TelaVenda();
        Venda.setVisible(true);
        this.dispose();
    }
    
    private void AplicarDesconto(){
        float ValorDescontado = ValorTotal * Desconto / 100;
        float ValorFinal = ValorTotal - ValorDescontado;
        
        setLabelDesconto(true);
        
        LblDescontoN.setText(String.valueOf("R$" + ValorDescontado));
        LblPrecoFinalN.setText("R$" + String.valueOf(String.valueOf(ValorFinal)));
        TxtDesconto.setText("");
        
    }
    
    private boolean isDescontoOK(){
        String Mensagem = "";
        
        if(!isStringNumeric(TxtDesconto.getText())){
            Mensagem = "Desconto invalido";
        }else{
            Desconto = Float.valueOf(TxtDesconto.getText());
            if(Desconto <= 0 || Desconto > 100){
                Mensagem = "Desconto deve ser entre 0 e 100%";
            }  
        }
        
        if(Mensagem.equals("")){
            return true;
        }else{
            TxtDesconto.setText("");
            JOptionPane.showMessageDialog(null, Mensagem);
            return false;
        }
        
        
    }
    
    private boolean isRecebidoOK(){
        String Mensagem = "";
        
        
        if(!isStringNumeric(TxtRecebido.getText())){
            Mensagem = "Valor recebido invalido";
        }else{
            Recebido = Float.valueOf(TxtRecebido.getText());
            if(Recebido < Float.valueOf(LblPrecoFinalN.getText().substring(2))){
                Mensagem = "Valor recebido menor que valor da venda";
            }
        }
        
        
        
        if(Mensagem.equals("")){
            return true;
        }else{
            TxtDesconto.setText("");
            JOptionPane.showMessageDialog(null, Mensagem);
            return false;
        }
    }
    
    private boolean isStringNumeric(String s){
      
        try {
            Double.parseDouble(s);
            return true;
        } catch (NumberFormatException ex) {
            return false;
        }
    } 
    
    private void setLabelTroco(boolean Opcao){
        LblRecebido.setVisible(Opcao);
        TxtRecebido.setVisible(Opcao);
        LblTroco.setVisible(Opcao);
        LblTrocoN.setVisible(Opcao);
    }
    
    private void setLabelDesconto(boolean Opcao){
        LblDescontoN.setVisible(Opcao);
        LblDescontoTotal.setVisible(Opcao);  

    }
    
    private void LimparTela(){
        BtnGroupPagamento.clearSelection();
        setLabelTroco(false);
        setLabelDesconto(false);
        LblPrecoFinalN.setText(LblTotalVenda.getText());
        TxtRecebido.setText("");
    }
    
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(TelaPagamento.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(TelaPagamento.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(TelaPagamento.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(TelaPagamento.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new TelaPagamento().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton BtnAplicarDesconto;
    private javax.swing.JButton BtnFinalizar;
    private javax.swing.ButtonGroup BtnGroupPagamento;
    private javax.swing.JButton BtnLimpar;
    private javax.swing.JButton BtnVoltar;
    private javax.swing.JRadioButton JRadioBoleto;
    private javax.swing.JRadioButton JRadioCredito;
    private javax.swing.JRadioButton JRadioDebito;
    private javax.swing.JRadioButton JRadioPix;
    private javax.swing.JLabel LblDesconto;
    private javax.swing.JLabel LblDescontoN;
    private javax.swing.JLabel LblDescontoTotal;
    private javax.swing.JLabel LblPagamento;
    private javax.swing.JLabel LblPrecoFinal;
    private javax.swing.JLabel LblPrecoFinalN;
    private javax.swing.JLabel LblRecebido;
    private javax.swing.JLabel LblTotal;
    private javax.swing.JLabel LblTotalVenda;
    private javax.swing.JLabel LblTroco;
    private javax.swing.JLabel LblTrocoN;
    private javax.swing.JPanel PnlBackGround;
    private javax.swing.JPanel PnlBotoes;
    private javax.swing.JPanel PnlCondicoes;
    private javax.swing.JPanel PnlDesconto;
    private javax.swing.JPanel PnlFinalizar;
    private javax.swing.JPanel PnlInfoVenda;
    private javax.swing.JTextField TxtDesconto;
    private javax.swing.JTextField TxtRecebido;
    private javax.swing.JRadioButton jRadioDinheiro;
    // End of variables declaration//GEN-END:variables
}
