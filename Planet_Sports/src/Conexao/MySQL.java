
package Conexao;

import java.sql.Connection;
import java.sql.Statement;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

public class MySQL {
    
    private Connection conn = null;
    private Statement statement;
    private ResultSet resultSet;
    
    private String servidor = "localhost:3306";
    private String nomeBanco = "planetsportsbd";
    private String usuario = "root";
    private String senha = "root";

    public MySQL(){
        
    }
    
    public MySQL(String servidor, String nomeDoBanco, String usuario, String senha){
        this.servidor = servidor;
        this.nomeBanco = nomeDoBanco;
        this.usuario = usuario;
        this.senha = senha;
    }
    
    public Connection getConn() {
        return conn;
    }

    public void setConn(Connection conn) {
        this.conn = conn;
    }

    public Statement getStatement() {
        return statement;
    }

    public void setStatement(Statement statement) {
        this.statement = statement;
    }

    public ResultSet getResultSet() {
        return resultSet;
    }

    public void setResultSet(ResultSet resultSet) {
        this.resultSet = resultSet;
    }
    
    public void conectaBanco(){
        try {
            
            conn = DriverManager.getConnection("jdbc:mysql://" + servidor + "/" + nomeBanco, usuario, senha);
            
            if (conn != null){
                System.out.println("Conexao efetuada com sucesso! " + "ID: " + conn );
            }
            
        } catch (Exception e){
            System.out.println("Conexao nao realizada - ERRO: " + e.getMessage());
        }
    }
    
    public boolean fechaBanco(){
        try {
            conn.close();
            return true;
        } catch (Exception e){
            System.out.println("Erro ao fechar banco  - ERRO: " + e.getMessage());
            return false;
        }
    }
    
    public int insertSQL(String SQL){
        int Status = 0;
        try {
            this.setStatement(getConn().createStatement());
            
            this.getStatement().executeUpdate(SQL);
            
            return Status;
        }catch (SQLException ex){
            ex.printStackTrace();
            return Status;
        }
    }
    public void executarSQL(String sql){
        try {
            this.statement = conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
            this.resultSet = this.statement.executeQuery(sql);
        }catch (SQLException sqlex){
            sqlex.printStackTrace();
        }
    }
    
    public boolean updateSQL(String pSQL){
    try {            
        //createStatement de con para criar o Statement
        this.setStatement(getConn().createStatement());

        // Definido o Statement, executamos a query no banco de dados
        getStatement().executeUpdate(pSQL);

    } catch (SQLException ex) {
        ex.printStackTrace();
        return false;
    }
    return true;
   }
}


