
package Objetos;


public class Produto {
    int Codigo;
    String Produto;
    String Descricao;
    float Preco;
    float Custo;
    String Esporte;
    
    public int getCodigo() {
        return Codigo;
    }

    public void setCodigo(int Codigo) {
        this.Codigo = Codigo;
    }
    
    public String getProduto() {
        return Produto;
    }

    public void setProduto(String Produto) {
        this.Produto = Produto;
    }

    public String getDescricao() {
        return Descricao;
    }

    public void setDescricao(String Descricao) {
        this.Descricao = Descricao;
    }

    public float getPreco() {
        return Preco;
    }

    public void setPreco(float Preco) {
        this.Preco = Preco;
    }

    public float getCusto() {
        return Custo;
    }

    public void setCusto(float Custo) {
        this.Custo = Custo;
    }

    public String getEsporte() {
        return Esporte;
    }

    public void setEsporte(String Esporte) {
        this.Esporte = Esporte;
    }
    
    public void Limpar(){
        this.Produto = "";
        this.Descricao= "";
        this.Preco = 0;
        this.Custo = 0;
        this.Esporte= "";
    }
}
