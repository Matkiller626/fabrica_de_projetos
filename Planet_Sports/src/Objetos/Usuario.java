
package Objetos;


public class Usuario {
    int     idUsuario;
    String  Username;
    String  Senha;
    int     Permissao;

    public String getUsername() {
        return Username;
    }

    public void setUsername(String Username) {
        this.Username = Username;
    }

    public String getSenha() {
        return Senha;
    }

    public void setSenha(String Senha) {
        this.Senha = Senha;
    }

    public int getPermissao() {
        return Permissao;
    }

    public void setPermissao(int Permissao) {
        this.Permissao = Permissao;
    }
    
    public boolean isAdmin(){
        return Permissao == 1;
    }
}
