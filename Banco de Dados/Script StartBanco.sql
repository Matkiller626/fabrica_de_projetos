drop database if exists PlanetSportsBD;
create database 		PlanetSportsBD;

use PlanetSportsBD;

drop table if exists	Usuario; 
drop table if exists	Cliente;
drop table if exists	Endereco; 
drop table if exists	Produto;

create table Usuario (
	idUsuario 	int			not null 	auto_increment primary key,
    Username	varchar(15)	not null 	unique,
    Senha		varchar(15)	not null,
    Permissao	tinyint		not null
);

create table Produto (
	idProduto	int				not null 	auto_increment primary key,
    Produto		varchar(150)	not null,
    Descricao	varchar(350)	not null,
    Preco		decimal(6,2)	not null,
    Custo 		decimal(6,2)	not null,
    Esporte		varchar(25)		
);

create table Endereco (
	idEndereco	int				not null 	auto_increment primary key,
    Rua			varchar(150)	not null,
    Numero		int,
    Bairro		varchar(100)	not null,
    Cidade 		varchar(100)	not null,
    UF			varchar(2)		not null
);

create table Cliente (
	idCliente		int				not null 	auto_increment primary key,
    NomeCompleto	varchar(150)	not null,
    Documento		varchar(15) 	not null,
    Nascimento		varchar(10)		not null,
    Telefone 		varchar(20),
    
    Endereco_id		int references Endereco(idEndereco)
);

insert into Usuario values (null, 'admin', 'admin', 1);